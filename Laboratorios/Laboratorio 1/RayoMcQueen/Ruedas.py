#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# Se crea la clase "Ruedas" para evaluar el desgaste que
# sufre cada rueda (Aleatoriamente) después de su uso.
class Ruedas:

    # Constructor.
    def __init__(self, rueda_1, rueda_2, rueda_3, rueda_4):
        self._rueda_1 = rueda_1
        self._rueda_2 = rueda_2
        self._rueda_3 = rueda_3
        self._rueda_4 = rueda_4


    # Métodos get
    def get_rueda_1(self):
        return self._rueda_1


    def get_rueda_2(self):
        return self._rueda_2


    def get_rueda_3(self):
        return self._rueda_3


    def get_rueda_4(self):
        return self._rueda_4


    # Métodos set
    # A cada valor se le suma un desgasto aleatorio después de su uso.
    def set_rueda_1(self, desgasto):
        self._rueda_1 = self._rueda_1 + desgasto


    def set_rueda_2(self, desgasto):
        self._rueda_2 = self._rueda_2 + desgasto


    def set_rueda_3(self, desgasto):
        self._rueda_3 = self._rueda_3 + desgasto


    def set_rueda_4(self, desgasto):
        self._rueda_4 = self._rueda_4 + desgasto
