#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class McQueen:

    # Constructor.
    def __init__(self, nombre, numero, motor, estanque, velocimetro,
                 kilometros, estado):
        self._nombre = nombre
        self._numero = numero
        self._motor = motor
        self._estanque = estanque
        self._velocimetro = velocimetro
        self._kilometros = kilometros
        self._estado = estado

    # Métodos get
    def get_nombre(self):
        return self._nombre


    def get_numero(self):
        return self._numero


    def get_motor(self):
        return self._motor


    def get_estanque(self):
        return self._estanque


    def get_velocimetro(self):
        return self._velocimetro


    def get_kilometros(self):
        return self._kilometros


    def get_estado(self):
        return self._estado


    # Métodos set
    def set_estanque(self, bencina):
        self._estanque = bencina


    def set_kilometros(self, kilometros_recorridos):
        self._kilometros = kilometros_recorridos


    def set_velocimetro(self, velocidad_actual):
        self._velocimetro = velocidad_actual


    def set_estado(self, nuevo_estado):
        self._estado = nuevo_estado
