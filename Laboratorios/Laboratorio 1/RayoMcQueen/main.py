#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Rayo import McQueen
from Ruedas import Ruedas
from random import randint, randrange
import os

# Función que recibe el estado inicial del auto y de sus ruedas
# para trabajar con estos parámetros.
def iniciar(cuchau, ruedas):

    print("\n")
    print("Ahora llega la hora de manejar!!!")
    print("\n")
    print("E/e: Para encender el auto")
    print("A/a: Para arrepentirse")

    print("\n")
    opcion = str(input("Ingrese la acción a realizar: "))

    seguir = True

    while(seguir == True):

        if(opcion.upper() == 'E'):
            # Se enciende el auto y su estado cambia a 'Encendido'
            estado_nuevo = 'Encendido'
            cuchau.set_estado(estado_nuevo)
            # El estanque pierde 1% de la bencina, ya que
            # se ha prendido el motor.
            bencina = (cuchau._estanque * 0.99)
            # Se actualiza el valor de estanque.
            cuchau.set_estanque(bencina)

            # Velocidad para motor 1.2
            velocidad_m_0 = randrange(0, 120, 20)
            # Velocidad para motor 1.6
            velocidad_m_1 = randrange(0, 120, 14)
            # Duración del viaje.
            tiempo_viaje = randrange(1, 10)

            # Desgaste de las ruedas generado de forma
            # random e individual después de cada uso.
            rueda_1 = randrange(1, 10)
            rueda_2 = randrange(1, 10)
            rueda_3 = randrange(1, 10)
            rueda_4 = randrange(1, 10)

            # Acciones para motor 1.2
            if (cuchau._motor == 1.2):

                distancia_recorrida = velocidad_m_0 * tiempo_viaje
                # Se cambia el valor de velocidad por la que se utilizará.
                cuchau.set_velocimetro(velocidad_m_0)
                # Cada 20 Km se consume 1 L de bencina.
                # De esta forma se obtiene el total de L de bencina
                # utilizados al recorrer la distancia.
                bencina_usada = distancia_recorrida/20
                bencina_m_0 = cuchau._estanque - bencina_usada
                # Se cambia el valor de la bencina del estanque.
                cuchau.set_estanque(bencina_m_0)

                # Cambio del estado de las ruedas producto del desgaste.
                ruedas.set_rueda_1(rueda_1)
                ruedas.set_rueda_2(rueda_2)
                ruedas.set_rueda_3(rueda_3)
                ruedas.set_rueda_4(rueda_4)

                # Impresión de cada uno de los atributos que posee
                # la clase 'Rayo'.
                if (cuchau._estanque > 0):
                    os.system('clear')
                    print("Su auto es: {0}"
                          "\nPosee el número: {1}"
                          "\nTiene un motor de: {2}"
                          "\nHa recorrido: {3} Km"
                          "\nVa a una velocidad de: {4} Km/h"
                          "\nLe quedan diponibles {5} L de bencina"
                          "\nLos daños de sus ruedas son: "
                          "\nRueda 1: {6}%"
                          "\nRueda 2: {7}%"
                          "\nRueda 3: {8}%"
                          "\nRueda 4: {9}%".format(cuchau.get_nombre(),
                                                   cuchau.get_numero(),
                                                   cuchau.get_motor(),
                                                   distancia_recorrida,
                                                   cuchau.get_velocimetro(),
                                                   cuchau.get_estanque(),
                                                   ruedas.get_rueda_1(),
                                                   ruedas.get_rueda_2(),
                                                   ruedas.get_rueda_3(),
                                                   ruedas.get_rueda_4()))

                    # Se verifica el estado del auto
                    #print("Su auto se encuentra: "
                    #      "{0}".format(cuchau.get_estado()))

                    print("\n")
                    print("¿Desea continuar avanzando?")
                    print("S/s: Para seguir")
                    print("Cualquier otra tecla para salir")
                    print("\n")

                    eleccion = input("Ingrese su elección: ")
                    print("\n")

                    if(eleccion.upper() == 'S'):
                        seguir = True

                    else:
                        os.system('clear')
                        print("Has decidido bajarte del auto en medio"
                              " de la nada")
                        print("Tu auto se va y te abandona")
                        seguir = False

                else:
                    os.system('clear')
                    print("Te has quedado sin la bencina necesaria para"
                          " moverte campeón")
                    print("Hasta aquí llegamos")
                    # Se apaga el auto producto de la pana de bencina
                    # y su estado cambia a 'Apagado'
                    estado_nuevo = 'Apagado'
                    cuchau.set_estado(estado_nuevo)
                    seguir = False

            # Acciones para motor 1.6
            elif(cuchau._motor == 1.6):

                distancia_recorrida = velocidad_m_1 * tiempo_viaje
                # Se cambia el valor de velocidad por la que se utilizará.
                cuchau.set_velocimetro(velocidad_m_1)
                # Cada 14 Km se consume 1 L de bencina.
                # De esta forma se obtiene el total de L de bencina
                # utilizados al recorrer la distancia.
                bencina_usada = distancia_recorrida/14
                bencina_m_1 = cuchau._estanque - bencina_usada
                # Se cambia el valor de la bencina del estanque.
                cuchau.set_estanque(bencina_m_1)

                # Cambio del estado de las ruedas producto del desgaste.
                ruedas.set_rueda_1(rueda_1)
                ruedas.set_rueda_2(rueda_2)
                ruedas.set_rueda_3(rueda_3)
                ruedas.set_rueda_4(rueda_4)

                # Impresión de cada uno de los atributos que posee
                # la clase 'Rayo'.
                if (cuchau._estanque > 0):
                    os.system('clear')
                    print("Su auto es: {0}"
                          "\nPosee el número: {1}"
                          "\nTiene un motor de: {2}"
                          "\nHa recorrido: {3} Km"
                          "\nVa a una velocidad de: {4} Km/h"
                          "\nLe quedan diponibles {5} L de bencina"
                          "\nLos daños de sus ruedas son: "
                          "\nRueda 1: {6}%"
                          "\nRueda 2: {7}%"
                          "\nRueda 3: {8}%"
                          "\nRueda 4: {9}%".format(cuchau.get_nombre(),
                                                   cuchau.get_numero(),
                                                   cuchau.get_motor(),
                                                   distancia_recorrida,
                                                   cuchau.get_velocimetro(),
                                                   cuchau.get_estanque(),
                                                   ruedas.get_rueda_1(),
                                                   ruedas.get_rueda_2(),
                                                   ruedas.get_rueda_3(),
                                                   ruedas.get_rueda_4()))

                    # Se verifica el estado del auto
                    #print("Su auto se encuentra: "
                    #      "{0}".format(cuchau.get_estado()))
                    print("\n")
                    sigo = print("¿Desea continuar avanzando?")
                    print("S/s: Para seguir")
                    print("Cualquier otra tecla para salir")
                    print("\n")

                    eleccion = input("Ingrese su elección: ")
                    print("\n")

                    if(eleccion.upper() == 'S'):
                        seguir = True

                    else:
                        os.system('clear')
                        print("Has decidido bajarte del auto en medio"
                              " de la nada")
                        print("Tu auto se va y te abandona")
                        seguir = False

                else:
                    os.system('clear')
                    print("Te has quedado sin la bencina necesaria para"
                          " moverte campeón")
                    print("Hasta aquí llegamos")
                    # Se apaga el auto producto de la pana de bencina
                    # y su estado cambia a 'Apagado'
                    estado_nuevo = 'Apagado'
                    cuchau.set_estado(estado_nuevo)
                    seguir = False

            else:
                pass

        elif(opcion.upper() == 'A'):
            os.system('clear')
            print("Preparate mentalmente y ven a por este reto en"
                  " otra oportunidad")
            # Verifica el estado del auto
            #print("Su auto se encuentra: {0}".format(cuchau.get_estado()))
            exit()

        else:
            os.system('clear')
            print("Ingrese una opción válida por favor")
            iniciar(cuchau, ruedas)


# Función encargada de recibir valores iniciales y de
# modificar el estado incial del auto.
def menu():

    os.system('clear')
    print("Bienvenido a las pruebas para la Piston Cup")
    print("            Customiza tu auto              ")
    print("\n")

    # Se corrobora que cada dato ingresado sea de tipo 'int' o 'float'.
    while True:
        try:
            nombre = str(input("Ingrese el nombre de su auto: "))
            numero = int(input("Ingrese el número de su auto: "))
            estanque = float(input("Ingrese la bencina que posee en estos"
                                   " momentos: "))
            velocimetro = int(input("Ingrese la velocidad a la que"
                                    " va su auto: "))
            km_recorridos = float(input("Ingrese los kilometros que ha"
                                        " recorrido: "))

            # Condicional para que se rompa el bucle (Se deben cumplir por
            # las normas que posee la Piston Cup).
            if((0 < estanque <= 32) and (0 < velocimetro <= 120)
                and (numero >= 0) and (km_recorridos >= 0)):
                break

            # Llama a la función normas para que el usuario ahora pueda
            # ingresar los valores correctos
            else:
                normas()

        except:
            pass

    # Obtención random del tipo de motor, 1.2 o 1.6
    random = randint(0, 1)
    if random == 0:
        motor = 1.2

    elif random == 1:
        motor = 1.6

    else:
        pass

    # Estado inicial del auto.
    estado = 'Apagado'
    cuchau = McQueen(nombre, numero, motor, estanque, velocimetro,
                     km_recorridos, estado)
    # Asignación desgaste inicial de las ruedas.
    ruedas = Ruedas(0, 0, 0, 0)

    iniciar(cuchau, ruedas)


# Función en la cual se definen los parámetros establecidos
# para poder iniciar el 'juego'.
def normas():

    os.system('clear')
    print("Normas para ingresar en el Piston Cup")
    print("Solo se permitirá el ingreso a los autos que posean las siguientes"
          " características")
    print("\n")
    print("1.- Su auto debe tener un número natural (Se acepta el 0)")
    print("2.- Todos los autos deberán tener una capacidad de 32 L como máximo"
          " en su estanque. Además, deberán ser L 'enteros'")
    print("3.- Todos los autos deberán tener una velocidad entre 0 y 120 Km/h")
    print("\n")

    print("M/m: Si desea volver al menú princpal")
    print("J/j: Para volver a ingresar los datos")
    print("Cualquier otra tecla si se ha aburrido")
    print("\n")

    opc = input("Ingrese su elección: ")
    print("\n")

    if(opc.upper() == 'M'):
        main()

    elif(opc.upper() == 'J'):
        manu()

    else:
        print("Esperamos que vuelva pronto")
        exit()


def main():

    os.system('clear')
    print("J/j: Para 'jugar'")
    print("N/n: Para leer las normas")
    print("Cualquier otra tecla para salir")

    print("\n")
    opc = str(input("Seleccione una opción: "))

    if(opc.upper() == 'J'):
        menu()

    elif(opc.upper() == 'N'):
        normas()

    else:
        exit()


if __name__ == '__main__':
    main()
