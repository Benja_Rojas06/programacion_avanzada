#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Levanta la ventana tipo message dialog question.
class Question():


    # Constructor
    def __init__(self, object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('lab1_gtk.ui')

        self.question = self.builder.get_object(object_name)
        self.question.set_title("Sumatoria de Números Enteros")
        self.question.show_all()

        # Definición del botón que se encargará de cerrar la ventana.
        self.btn_cancel = self.builder.get_object('btnCancel')
        self.btn_cancel.connect('clicked', self.btn_cancel_clicked)

        # Definición del botón que se encargará de eliminar los elementos.
        self.btn_ok = self.builder.get_object('btnOk')
        self.btn_ok.connect('clicked', self.btn_ok_clicked)


    # Cierra la ventana en caso de presionar botón "Cancel".
    def btn_cancel_clicked(self, btn=None):
        self.question.destroy()


    # Borra lo que hay en los cuadros de texto al presionar botón "Ok".
    def btn_ok_clicked(self, btn=None):
        # No se logra lo solicitado.
        self.question.destroy()
