#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from wnInfo import Info
from wnQuestion import Question

# Clase que posee las opciones del apartado "Sumatoria".
class Sumatoria():


    # Constructor
    def __init__(self, object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('lab1_gtk.ui')

        self.sumatoria = self.builder.get_object(object_name)
        self.sumatoria.set_title("Sumatoria de Números Enteros")
        self.sumatoria.show_all()

        # Definición de los "entry".
        self.number_one = self.builder.get_object("number1")
        self.number_two = self.builder.get_object("number2")
        self.number_three = self.builder.get_object("number3")
        self.number_four = self.builder.get_object("number4")
        self.number_result = self.builder.get_object("result")

        # Definición del botón "Sumar".
        boton_sumar = self.builder.get_object('btnSumar')
        boton_sumar.connect('clicked', self.boton_sumar_clicked)

        # Definición del botón "Aceptar".
        boton_accept = self.builder.get_object('btnAccept')
        boton_accept.connect('clicked', self.boton_accept_clicked)

        # Definición del botón "Refresh".
        boton_refresh = self.builder.get_object('btnRefresh')
        boton_refresh.connect('clicked', self.boton_refresh_clicked)


    # Suma los números en caso de presionar "Sumar".
    def boton_sumar_clicked(self, btn=None):
        # No se logra reconversión de texto a número entero mediante
        # función length, es por ello que solo se pueden sumar números.
        num1 = self.number_one.get_text()
        num2 = self.number_two.get_text()
        num3 = self.number_three.get_text()
        num4 = self.number_four.get_text()
        resultado = float(num1) + float(num2) + float(num3) + float(num4)
        # Cambia el resultado a tipo str al momento de sumar.
        self.number_result.set_text(str(resultado))


    # Abre ventana message dialog info al presionar "Aceptar".
    def boton_accept_clicked(self, btn=None):
        wnInfo = Info(object_name='wnInfo')
        wnInfo.info.run()
        wnInfo.info.destroy()


    # Abre ventana message dialog question al presionar "Refresh".
    def boton_refresh_clicked(self, btn=None):
        wnQuestion = Question(object_name='wnQuestion')
        wnQuestion.question.run()
        wnQuestion.question.destroy()
