#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Clase con la información acerca del nombre del laboratorio,
# módulo, unidad correspondiente, nombre del docente a cargo
# y del creador. Además, posee la imagen del símbolo de suma.
class About():


    # Constructor
    def __init__(self, object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('lab1_gtk.ui')

        self.about = self.builder.get_object(object_name)
        self.about.show_all()

        # Definición del botón que se encargará de cerrar la ventana.
        self.btn_close = self.builder.get_object('btnClose')
        self.btn_close.connect('clicked', self.btn_close_clicked)


    # Cierra la ventana en caso de presionar botón "Close".
    def btn_close_clicked(self, btn=None):
        self.about.destroy()
