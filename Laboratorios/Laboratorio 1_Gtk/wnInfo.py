#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Levanta la ventana tipo message dialog info.
class Info():


    # Constructor
    def __init__(self, object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('lab1_gtk.ui')

        self.info = self.builder.get_object(object_name)
        self.info.set_title("Sumatoria de Números Enteros")

        # Definición del botón que se encargará de cerrar la ventana.
        self.btnOk = self.builder.get_object('btnOK')
        self.btnOk.connect('clicked', self.btnOk_clicked)

        # TreeNumber.
        self.treeNumber = self.builder.get_object("treeNumber")
        # Definición del modelo en tipo lista.
        self.tree_model = Gtk.ListStore(*(5 * [str]))
        # Se agrega el modelo al objeto TreeNumeros.
        self.treeNumber.set_model(model=self.tree_model)

        # Definición de columna tipo texto.
        column = Gtk.CellRendererText()

        # Columna 1.
        columna = Gtk.TreeViewColumn(title="N° 1",
                                    cell_renderer=column,
                                    text=0)
        # Se agrega la columna.
        self.treeNumber.append_column(columna)

        # Columna 2.
        columna = Gtk.TreeViewColumn(title="N° 2",
                                    cell_renderer=column,
                                    text=1)
        # Se agrega la columna.
        self.treeNumber.append_column(columna)

        # Columna 3.
        columna = Gtk.TreeViewColumn(title="N° 3",
                                    cell_renderer=column,
                                    text=2)
        # Se agrega la columna.
        self.treeNumber.append_column(columna)

        # Columna 4.
        columna = Gtk.TreeViewColumn(title="N° 4",
                                    cell_renderer=column,
                                    text=3)
        # Se agrega la columna.
        self.treeNumber.append_column(columna)

        # Columna 5.
        columna = Gtk.TreeViewColumn(title="Resultado",
                                    cell_renderer=column,
                                    text=4)
        # Se agrega la columna.
        self.treeNumber.append_column(columna)

        # Columnas "iniciales".
        self.tree_model.append(["", "", "", "", ""])

        self.info.show_all()

    # Cierra la ventana en caso de presionar botón 'OK'.
    def btnOk_clicked(self, btn=None):
        self.info.destroy()
