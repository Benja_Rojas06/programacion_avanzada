#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from wnAbout import About
from wnSumatoria import Sumatoria


# Clase principal de la cual sale "todo".
class Principal():


    # Constructor.
    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('lab1_gtk.ui')

        main_window = self.builder.get_object('wnMain')
        # Tamaño máximo para la ventana principal.
        main_window.maximize()
        # Título de la ventana principal.
        main_window.set_title("Sumatoria de Números Enteros")
        # Cierre de ventana principal.
        main_window.connect('destroy', Gtk.main_quit)

        # Creación btnAbout.
        boton_about = self.builder.get_object('btnAbout')
        boton_about.connect('clicked', self.boton_about_clicked)

        # Creación btnSumatoria.
        boton_sumatoria = self.builder.get_object('btnSumatoria')
        boton_sumatoria.connect('clicked', self.boton_sumatoria_clicked)

        # TreeNumeros.
        self.treeNumeros = self.builder.get_object("treeNumeros")
        # Definición del modelo en tipo lista.
        self.tree_model = Gtk.ListStore(*(5 * [str]))
        # Se agrega el modelo al objeto TreeNumeros.
        self.treeNumeros.set_model(model=self.tree_model)

        # Definición de columna tipo texto.
        column = Gtk.CellRendererText()

        # Columna 1.
        columna = Gtk.TreeViewColumn(title="N° 1",
                                    cell_renderer=column,
                                    text=0)
        # Se agrega la columna.
        self.treeNumeros.append_column(columna)

        # Columna 2.
        columna = Gtk.TreeViewColumn(title="N° 2",
                                    cell_renderer=column,
                                    text=1)
        # Se agrega la columna.
        self.treeNumeros.append_column(columna)

        # Columna 3.
        columna = Gtk.TreeViewColumn(title="N° 3",
                                    cell_renderer=column,
                                    text=2)
        # Se agrega la columna.
        self.treeNumeros.append_column(columna)

        # Columna 4.
        columna = Gtk.TreeViewColumn(title="N° 4",
                                    cell_renderer=column,
                                    text=3)
        # Se agrega la columna.
        self.treeNumeros.append_column(columna)

        # Columna 5.
        columna = Gtk.TreeViewColumn(title="Resultado",
                                    cell_renderer=column,
                                    text=4)
        # Se agrega la columna.
        self.treeNumeros.append_column(columna)

        self.tree_model.append(["", "", "", "", ""])

        # Muestra la pantalla.
        main_window.show_all()


    # Función btnAbout.
    def boton_about_clicked(self, btn=None):
        wnAbout = About(object_name='wnAbout')
        wnAbout.about.run()
        wnAbout.about.destroy()


    # Función btnSumatoria.
    def boton_sumatoria_clicked(self, btn=None):
        wnSumatoria = Sumatoria(object_name='wnSumatoria')
        wnSumatoria.sumatoria.run()
        wnSumatoria.sumatoria.destroy()


# Función Main
if __name__ == '__main__':

    Principal()
    Gtk.main()
