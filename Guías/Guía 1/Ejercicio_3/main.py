#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Encriptar import Encriptar


# Función encargada del cambio de posición final.
def cambio_posicion(encrip_final, numero_a_lista):

    # Cambio de las posiciones de los elementos de la lista.
    encrip_final[0], encrip_final[2] = encrip_final[2], encrip_final[0]
    encrip_final[1], encrip_final[3] = encrip_final[3], encrip_final[1]

    # Se asignan los valores a la clase.
    numero_encriptado = Encriptar(numero_a_lista, encrip_final)

    print("El número a encriptar es: ", numero_encriptado.get_inicial())
    print("El número resultante de la encriptación es: ",
          numero_encriptado.get_final())


# Función encargada de sumar 7 a cada elemento.
def suma_7(lista_sum):

    suma = 0
    lista_suma7 = []

    # Se recorre el largo de la lista.
    for i in range(len(lista_sum)):
        # A cada elemento se le suma 7.
        # Módulo utilizado para los valores mayores de 10.
        # De esta forma, un 11 pasa a ser un 1, un 12 un 2 y aśi sucesivamente.
        suma = (7 + lista_sum[i]) % 10
        lista_suma7.append(suma)

    return lista_suma7


# Función encargada de sumar el elemento "actual" con los anteriores.
def suma_continua(lista):

    suma = 0
    lista_sumas = []

    # Se recorre el largo de la lista.
    for i in range(len(lista)):
        # Se suman los elementos por posición.
        # Módulo utilizado para los valores mayores de 10.
        # De esta forma, un 11 pasa a ser un 1, un 12 un 2 y aśi sucesivamente.
        suma = (suma + lista[i]) % 10
        lista_sumas.append(suma)

    return lista_sumas


# Función en la que se solicita el numero a ser encriptado.
def encriptacion():

    numero = int(input("Ingrese un número de 4 digitos: "))

    while True:

        try:
            if(numero >= 1000 and numero < 10000):
                # Transforma un número en una lista con sus digitos.
                numero_a_lista = [int(n) for n in str(numero)]
                break

            else:
                pass

        except:
            print("El número ingresado no es válido, favor ingrese nuevamente")

    encrip1 = suma_continua(numero_a_lista)
    encrip2 = suma_7(encrip1)
    cambio_posicion(encrip2, numero_a_lista)


if __name__ == '__main__':
    encriptacion()
