#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Encriptar:


    # Constructor.
    def __init__(self, primer_numero, ultimo_numero):
        self._primer_numero = primer_numero
        self._ultimo_numero = ultimo_numero


    # Función definicda para get.
    def get_inicial(self):
        return self._primer_numero


    def get_final(self):
        return self._ultimo_numero
