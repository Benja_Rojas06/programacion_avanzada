#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Tarjeta import Tarjeta


# Función que recibe un nombre y un saldo.
def ingreso_datos():

    nombre = str(input("Ingrese el nombre del titular de la tarjeta: "))

    # Se corrobora que el valor ingresado sea un número.
    while True:
        try:
            saldo = float(input("Ingrese el saldo de la tarjeta: "))
            if saldo >= 0:
                break

            else:
                pass

        except:
            print("\nIngrese un valor válido por favor\n")

    # Se envían los valores a la clase "Tarjeta"
    titular = Tarjeta(nombre, saldo)

    print("El titular de la tarjeta es: {0}"
    " y su saldo actual es: {1}".format(titular.get_titular(),
                                        titular.get_saldo()))

    while True:
        try:
            comprar = float(input("\nIngrese el valor de la compra"
                                  " ha realizar: "))
            # Condicional para que el valor de la compra
            # no sea mayor al del saldo. De ser así, se
            # entra en bucle.
            if (titular._saldo >= comprar):
                saldo_nuevo = titular._saldo - comprar
                titular.set_saldo(saldo_nuevo)
                print("Su nuevo saldo es de: {0}".format(titular.get_saldo()))
                break

            else:
                print("\nERROR")
                print("No dispone del saldo suficiente para realizar la compra")
                print("\n")

        except:
            pass


if __name__ == '__main__':
    ingreso_datos()
