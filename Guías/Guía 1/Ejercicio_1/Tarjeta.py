#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Tarjeta():

    # Constructor
    def __init__(self, titular, saldo):
        self._titular = titular
        self._saldo = saldo

    # Función definida para get.
    def get_titular(self):
        return self._titular


    def get_saldo(self):
        return self._saldo

    # Función definida para set, que, además
    # puede cambiar el valor del saldo.
    def set_saldo(self, saldo_nuevo):
        self._saldo = saldo_nuevo
