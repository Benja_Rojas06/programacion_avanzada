#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from random import randrange
from Perro import Perro


# Función que pide los datos del perro y le davida.
def dog_lover():

    # Atributos de nuestro perro.
    nombre = str(input("Ingrese el nombre de su perro: "))
    color = str(input("Ingrese el color de su perro: "))
    raza = str(input("Ingrese la raza de su perro: "))
    genero = str(input("Ingrese el genero de su perro: "))
    pelaje = str(input("Ingrese el pelaje de su perro (Corto, largo): "))
    tamanio = str(input("Ingrese el tamaño de su perro (Grande, Chico): "))

    while True:
        try:
            edad = int(input("Ingrese la edad de su perro (En años humanos): "))
            peso = float(input("Ingrese el peso del perro (En Kg): "))
            break

        except:
            print("Ha ingresado un dato erróneo")

    # Envía los datos a la clase "Perro".
    dog = Perro(nombre, color, raza, edad, genero, peso, pelaje, tamanio)

    print("Su perro ha hecho/pensado todo esto en tan solo unos segundos")

    # Condicionales para que el perro tenga vida por sí "solo".
    for i in range(13):
        accion_perro = randrange(1,13)

        if(accion_perro == 1):
            print("\n")
            dog.ladrar()

        elif(accion_perro == 2):
            print("\n")
            dog.jugar()

        elif(accion_perro == 3):
            print("\n")
            dog.comer()

        elif(accion_perro == 4):
            print("\n")
            dog.dormir()

        elif(accion_perro == 5):
            print("\n")
            dog.afilar_dientes()

        elif(accion_perro == 6):
            print("\n")
            dog.aullar()

        elif(accion_perro == 7):
            print("\n")
            dog.necesidades()

        elif(accion_perro == 8):
            print("\n")
            dog.beber()

        elif(accion_perro == 9):
            print("\n")
            dog.maldades()

        elif(accion_perro == 10):
            print("\n")
            dog.morder()

        elif(accion_perro == 11):
            print("\n")
            dog.tristeza()

        elif(accion_perro == 12):
            print("\n")
            dog.correr()

        elif(accion_perro == 13):
            print("\n")
            dog.amor()


if __name__ == '__main__':
    dog_lover()
