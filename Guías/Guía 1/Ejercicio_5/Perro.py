#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Perro:

    def __init__(self, nombre, color, raza, edad, genero,
                 peso, pelaje, tamanio):
        self.nombre = nombre
        self.color = color
        self.raza = raza
        self.edad = edad
        self.genero = genero
        self.peso = peso
        self.pelaje = pelaje
        self.tamanio = tamanio


    def ladrar(self):
        print("Un humano extraño, le ladraré")


    def jugar(self):
        print("No me gusta 'ir a buscar la bola', prefiero perseguirme la cola")


    def comer(self):
        print("No entiendo porque me dan esta cosa café si me pueden dar"
              " comida")


    def dormir(self):
        print("Ha sido un día largo, espero que no aparezca ningún humano")


    def afilar_dientes(self):
        print("Llegó la hora de comerme mi casa para tener"
              " unos dientes más fuertes")


    def aullar(self):
        print("Son las 12, hora del aullido masivo")


    def necesidades(self):
        print("Lo mejor de ser perro es que el mundo es tu baño")


    def beber(self):
        print("Cuando tengo sed, me tomo una olla")


    def maldades(self):
        print("Lo siento, no me quería comer tus calcetines")


    def morder(self):
        print("No sé porque piensan que cuando nos mordemos el cuello estamos"
              " peleando")


    def tristeza(self):
        print("Ya extraño a mi humano :c")


    def correr(self):
        print("Me encanta correr por todo el patio, aunque no sea muy grande")


    def amor(self):
        print("Estoy en la mejor familia :D")
