#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# Clase solicitada
class Libro:

    # Constructor
    def __init__(self, titulo, autor, ejem_existentes, ejem_prestados):
        self._titulo = titulo
        self._autor = autor
        self._ejem_existentes = ejem_existentes
        self._ejem_prestados = ejem_prestados


    # Funciones definidas para get.
    def get_titulo(self):
        return self._titulo


    def get_autor(self):
        return self._autor


    def get_ejem_existentes(self):
        return self._ejem_existentes


    def get_ejem_prestados(self):
        return self._ejem_prestados
