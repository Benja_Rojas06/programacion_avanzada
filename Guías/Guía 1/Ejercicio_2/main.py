#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Libro import Libro

# Función que agregrará los libros.
def agregar():

    # Lista en la cual se guardarán todos los libros
    # ingresados con sus respectivos datos.
    libros_ingresados = []

    # Corroboración de que se ingresa un número.
    while True:
        try:
            print("\n")
            cantidad_libros = int(input("Ingrese la cantidad de libros"
                                        " que desea agregar: "))
            break
        except:
            print("Ingrese un número por favor")

    for i in range(cantidad_libros):
        print("\n")
        titulo = str(input("Ingrese el título del libro: "))
        autor = str(input("Ingrese el autor del libro: "))

        # Corroboración de que se ingresa un número.
        while True:
            try:
                ejem_existentes = int(input("Ingrese la cantidad de ejemplares"
                                            " que posee actualmente: "))
                ejem_prestados = int(input("Ingrese la cantidad de ejemplares"
                                             " que ha prestado: "))
                break
            except:
                print("Ingrese un valor válido por favor")

        # Una vez obtenidos los datos, se envían a la clase 'Libro'.
        libro = Libro(titulo, autor, ejem_existentes, ejem_prestados)
        # Se guardan todos los datos ingresados en nuestra lista
        libros_ingresados.append(libro)

    print("\n")
    # Lista con todos los datos con la cual se
    # trabajará en todo el problema.
    return libros_ingresados


# Función que muestra un libro y autor solicitados por el usuario.
def buscar(libros):

    titulo_libro = str(input("Ingrese el título del libro: "))

    # Se recorre la lista con los libros
    # hasta encontra un match.
    for i in libros:
        while True:
            try:
                # Una vez se encuentra, se imprimen sus datos.
                if(i._titulo == titulo_libro):
                    print("El titulo del libro es: {0} y su autor "
                          "es: {1} ".format(i.get_titulo(), i.get_autor()))
                    print("\n")
                    break
            # Match inexistente
            except:
                print("Usted no posee ese libro")
    print("\n")


# Función que muestra los ejemplares disponibles de
# un libro (En poseción y prestados).
def ejemplares(libros):

    titulo_libro =  str(input("Ingrese el título del libro: "))

    # Se recorre la lista con los libros
    # hasta encontra un match.
    for i in libros:
        while True:
            try:
                # Una vez se encuentra, se imprimen sus datos.
                if(i._titulo == titulo_libro):
                    print("El titulo del libro es: {0}, usted posee {1}"
                          " ejemplares y ha prestado {2}"
                          " ejemplares".format(i.get_titulo(),
                                               i.get_ejem_existentes(),
                                               i.get_ejem_prestados()))
                    break
            # Match inexistente
            except:
                print("Usted no posee ese libro")
    print("\n")


# Función que permite visualizar los libros y sus respectivos datos.
def visualizar(libros):

    print("\n")
    print("Los datos de todos los libros que tiene disponibles son: ")
    # Recorre la lista creada con los libros ingresados
    # e impreme todos sus datos.
    for i in libros:
        print("Titulo: {0}"
              "\nAutor: {1}"
              "\nEjemplares existentes: {2}"
              "\nEjemplares prestados: {3}".format(i.get_titulo(),
                                                   i.get_autor(),
                                                   i.get_ejem_existentes(),
                                                   i.get_ejem_prestados()))
    print("\n")


# Menú con diversas opciones para que realiace el usuario.
def menu():

    while True:
        print("\n")
        print("Bienviendo a su biblioteca en línea, ¿Qué desea realizar?")
        print("Agregar un libro: A/a")
        print("Buscar un libro por su titulo: B/b")
        print("Cantidad de ejemplares disponibles y prestados por libro: E/e")
        print("Visualizar libros disponibles: V/v")
        print("Salir: S/s")
        print("\n")

        opc = str(input("Ingrese lo que desea realizar: "))

        # Agrega libros.
        if (opc.upper() == 'A'):
            libros = agregar()

        # Busca libros.
        elif(opc.upper() == 'B'):
            # Obliga a que el usuario haya ingresado un libro con anterioridad.
            try:
                buscar(libros)
            except:
                print("Usted no ha ingresado un libro todavía")

        # Busca cantidad de ejemplares.
        elif (opc.upper() == 'E'):
            # Obliga a que el usuario haya ingresado un libro con anterioridad.
            try:
                ejemplares(libros)
            except:
                print("Usted no ha ingresado un libro todavía")

        # Permite visualizar todos los libros ingresados.
        elif (opc.upper() == 'V'):
            # Obliga a que el usuario haya ingresado un libro con anterioridad.
            try:
                visualizar(libros)
            except:
                print("Usted no ha ingresado un libro todavía")

        # Cierra el programa.
        elif (opc.upper() == 'S'):
            exit()

        else:
            print("\nFavor ingresar una opción válida\n")


if __name__ == '__main__':
    menu()
