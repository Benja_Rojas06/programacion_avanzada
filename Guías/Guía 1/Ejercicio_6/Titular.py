#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Titular:


    def __init__(self, nombre, cuenta, saldo):
        self._nombre = nombre
        self._cuenta = cuenta
        self._saldo = saldo


    def get_nombre(self):
        return self._nombre


    def get_cuenta(self):
        return self._cuenta


    def get_saldo(self):
        return self._saldo


    # Método que cambia el saldo de la cuenta producto de un depósito.
    def set_deposito(self, deposito):
        # Depósito no puede ser 0 o menor ya que no se puede
        # depositar esa cantidad de dinero.
        if (deposito > 0):
            self._saldo = self._saldo + deposito
            print("Su depósito se ha realizado exitosamente")

        elif(deposito == 0):
            print("No se pueden depositar $0")

        elif(depositar < 0):
            print("No puede depositar una cantidad negativa")

        else:
            print("No ha ingresado una opción válida")


    # Método que cambia el saldo de la cuenta producto de un giro.
    def set_giro(self, giro):
        # No se pueden sacar 0 pesos ni tampoco una
        # cantidad mayor a la del saldo.
        if (giro > 0 and self._saldo >= giro):
            self._saldo = self._saldo - giro
            print("Su giro se ha realizado exitosamente")

        elif(giro == 0):
            print("No se pueden girar $0")

        elif(giro < 0):
            print("No puede girar una cantidad negativa")

        elif(giro > self._saldo):
            print("No dispone del saldo suficiente para realizar el giro")

        else:
            print("No ha ingresado una opción válida")


    # Método que cambia el saldo de la cuenta producto de una transferencia.
    def set_transferencia(self, titular_destinatario, transferencia):

        # Mismo caso que en el método "set_giro".
        if (transferencia > 0 and self._saldo >= transferencia):
            self._saldo = self._saldo - transferencia
            print("La transferencia se ha realizado exitosamente")

        elif(transferencia == 0):
            print("No se pueden girar $0")

        elif(transferencia < 0):
            print("No puede girar una cantidad negativa")

        elif(transferencia > self._saldo):
            print("No dispone del saldo suficiente para realizar el giro")

        else:
            print("No ha ingresado una opción válida")
