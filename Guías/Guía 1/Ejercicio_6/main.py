#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Titular import Titular


def menu():

    nombre = str(input("Ingrese su nombre: "))

    while True:
        try:
            cuenta = int(input("Ingrese su número de cuenta: "))
            saldo = int(input("Ingrese su saldo actual: "))
            break

        except:
            print("Ingrese un valor válido")

    titular = Titular(nombre, cuenta, saldo)

    while True:
        print("\n")
        print("Bienvenido a su banca en línea, ¿Qué operación desea realizar?")
        print("Para depositarse: D/d")
        print("Para girar: G/g")
        print("Para realizar una transferencia: T/t")
        print("Para visualizar sus datos personales personales: P/p")
        print("Para salir: S/s")
        print("\n")

        opc = str(input("Ingrese su operación: "))

        if(opc.upper() == 'D'):
            print("\n")
            while True:
                try:
                    deposito = int(input("Ingrese el monto que se quiere"
                                         " depositar: "))
                    titular.set_deposito(deposito)
                    break
                except:
                    print("Valor inválido, favor ingresar nuevamente")

        elif(opc.upper() == 'G'):
            print("\n")
            while True:
                try:
                    giro = int(input("Ingrese el monto que desea girar: "))
                    titular.set_giro(giro)
                    break
                except:
                    print("Valor inválido, favor ingresar nuevamente")

        elif(opc.upper() == 'T'):
            print("\n")
            while True:
                try:
                    destinatario = int(input("Ingrese el número de cuenta del"
                                             " destinatario: "))
                    monto = int(input("Ingrese el monto a transferir: "))
                    titular.set_transferencia(destinatario, monto)
                    break
                except:
                    print("Valor inválido, favor ingresar nuevamente")

        # Llama a los atributos asociados al inicio
        elif(opc.upper() == 'P'):
            print("\n")
            print("Nombre del titular: ", titular.get_nombre())
            print("Número de cuenta: ", titular.get_cuenta())
            print("Saldo disponible: ", titular.get_saldo())

        elif(opc.upper() == 'S'):
            exit()

        else:
            print("\nFavor ingresar una opción válida\n")


if __name__ == '__main__':
    menu()
