---
title: "Presencia de Enfermedad Cardíaca en Pacientes"
author: "Benjamín Rojas Rojas"
date: "8/5/2020"
output:
  pdf_document: default
  html_document: default
---

## Introducción:

La denominada enfermedad cardíaca o cardiovascular, es una serie de problemas 
que involucran tanto al corazón, como a los vasos sanguíneos. Esta enfermedad 
ocurre cuando la grasa y el colesterol se acumulan en las paredes de las 
arterias y, a esta, se le denomina "placa". Con el paso de los años, esta última 
puede estrechar las arterias u obstruirlas completamente y, de este modo, 
provocar una multitud de problemas en todo el cuerpo. De este mismo modo, si una 
arteria resulta obstruida (Se impide el paso de la sangre), podría provocar, 
eventualmente, un ataque cardíaco o un accidente cerebrovascular. 

A su vez, estas las podemos clasificar en:

- **Hipertensión Arterial (Presión alta).**
- **Cardiopatía Coronaria (Infarto de miocardio)**
- **Enfermedad Cerebrovascular (Apoplejía).**
- **Enfermedad Vascular Periférica.**
- **Insuficiencia Cardíaca.**
- **Cardiopatía Reumática.**
- **Cardiopatía Congénita.**
- **Miocardiopatías.**

En este ámbito, en el presente estudio se trabajó con 303 pacientes para 
comprobar si acaso estos tenían la presencia o ausencia de enfermedad cardíaca, 
cualquiera fuese su tipo. Para ello se trabajó con el dataset 
"datasets_heart.csv".

## Datos:

Como ya se mencionó anteriormente se trabajó con el dataset 
"datasets_heart.csv":

### Dataset "datasets_heart.csv":
```{r}
# Obtención del dataset.
cardiovascular_disease <- "datasets_heart.csv"

# Lectura del dataset y definición de cd_data ("Cardiovascular Disease Data).
cd_data <- read.csv(cardiovascular_disease, sep=",")

# print(cd_data)
```


En el cual se logran visualizar distintas características de los pacientes,  
representadas en columnas:

### Las diferentes columnas con las que se trabajó en el estudio:
```{r}
colnames(cd_data)
```


Además, y para interiorizarnos más con el dataset, cabe mencionar que sus 
características son:

### Dimensiones:
```{r}
dim(cd_data)
```

### Estructura de sus variables:
```{r}
str(cd_data)
```

### Resumen estadístico:
```{r}
summary(cd_data)
```


## Procedimiento:

A modo de tener una claridad del tipo de variable con el que se estaba 
trabajando, es que se clasificaron las variables del dataset como cuantitativas 
o cualitativas según les correspondiera.

### Variables Cuantitativas del dataset:

Debido a que el tipo de las variables era "interger", se debió cambiar a 
"numeric" en todas las variables cuantitavas:

```{r}
# Paso de las variables de tipo "int" a "numeric".
cd_data$age <- as.numeric(gsub(",", ".", as.character(cd_data$age)))
cd_data$trestbps <- as.numeric(gsub(",", ".", as.character(cd_data$trestbps)))
cd_data$chol <- as.numeric(gsub(",", ".", as.character(cd_data$chol)))
cd_data$restecg <- as.numeric(gsub(",", ".", as.character(cd_data$restecg)))
cd_data$thalach <- as.numeric(gsub(",", ".", as.character(cd_data$thalach)))
cd_data$oldpeak <- as.numeric(gsub(",", ".", as.character(cd_data$oldpeak)))
cd_data$slope <- as.numeric(gsub(",", ".", as.character(cd_data$slope)))
cd_data$ca <- as.numeric(gsub(",", ".", as.character(cd_data$ca)))

# Corroboración de que se han cambiado todos los tipos de variable.
str(cd_data)
```


Para las variables cualitativas también se realizó un procedimiento similar, ya 
que todas estas se cambiaron a tipo "factor" para que estas se trabajaran de la 
manera correcta:

### Variables Cualitativas del dataset:
```{r}
# Paso de las variables a tipo "factor".
cd_data$sex <- as.factor(cd_data$sex)
cd_data$cp <- as.factor(cd_data$cp)
cd_data$fbs  <- as.factor(cd_data$fbs)
cd_data$exang <- as.factor(cd_data$exang)
cd_data$thal <- as.factor(cd_data$thal)
cd_data$target <- as.factor(cd_data$target)

# Corroboración de que se han cambiado todos los tipos de variable.
str(cd_data)
```


Para tener claridad de con que tipo de paciente se estaba trabajando es que se 
realizaron subconjuntos de estos, para clasificarlos dentro de los mayores de 
edad (>= 18 años) y los menores de edad (< 18):

### Separación por rango de edad:
```{r}
# Subdataset para mayores de edad.
mayores18 <- subset(cd_data, age >= 18)
# Subdataset para menores de edad.
menores18 <- subset(cd_data, age < 18) 

print(mayores18$age)
print(menores18$age)
```


Gracias a esto se pudo saber que no existen pacientes menores de edad en este 
estudio.


## Resultados:

Se realizaron diversos tipos de gráficos con el fin de analizar a fondo los 
datos presentes en el dataset y, así, poder identificar, claramente, la 
frecuencia, distribución y reconocimiento de los valores outliers (Serán 
utlizados más adelante). Para ello se trabajó con las variables cuantitativas y 
cualitativas por separado con el fin de obtener un orden mayor a la hora de 
entregar los datos.

### Histogramas, Distribución de datos, Gráfico de caja y bigotes e identificación de valores outliers para Variables Cuantitativas:

### Edad:

```{r}
# Importe de libreria que permite trabajar con "ggplot".
library(ggplot2)

# Generación histograma numérico.
hist(as.numeric(cd_data$age), main = "Frecuencia de las edades")

# Generación de distribución de los datos.
ggplot(mapping=aes(x = cd_data$age)) + geom_density()

# Generación gráfico de caja y bigote y Reconocimiento de datos outliers.
age <- cd_data$age
outliers_age <- boxplot(age, main = "Edades de los pacientes")$out
print(outliers_age) # Valores outliers.
```

### Presión arterial en reposo:
```{r}
hist(as.numeric(cd_data$trestbps), main="Frecuencia presión arterial en reposo")
ggplot(mapping=aes(x = cd_data$trestbps)) + geom_density()

trestbps <- cd_data$trestbps
outliers_trestbps <- boxplot(trestbps, 
                             main="Presión arterial en reposo de pacientes")$out
print(outliers_trestbps)
```

### Suero colestoral en mg/dl:
```{r}
hist(as.numeric(cd_data$chol), main="Frecuencia suero colestoral")
ggplot(mapping=aes(x = cd_data$chol)) + geom_density()

chol <- cd_data$chol
outliers_chol <- boxplot(chol, main="Suero colestoral de pacientes")$out
print(outliers_chol)
```

### Resultados electrocardiográficos (R.E.) en reposo:
```{r}
hist(as.numeric(cd_data$restecg), main="Fecuencia de R.E. en reposo")
ggplot(mapping=aes(x = cd_data$restecg)) + geom_density()

restecg <- cd_data$restecg
outliers_restecg <- boxplot(restecg, main="R.E. en reposo de pacientes")$out
print(outliers_restecg)
```

### Frecuencia cardíaca (F.C.) máxima alcanzada:
```{r}
hist(as.numeric(cd_data$thalach), main="Fecuencia cardíaca máxima alcanzada")
ggplot(mapping=aes(x = cd_data$thalach)) + geom_density()

thalach <- cd_data$thalach
outliers_thalach <- boxplot(thalach, 
                            main="F.C. máxima alcanzada por pacientes")$out
print(outliers_thalach)
```

### Depresión del ST inducida por el ejercicio relativo al descanso:
```{r}
hist(as.numeric(cd_data$oldpeak), 
     main="Frecuencia de depresión del ST (Con descanso)")
ggplot(mapping=aes(x = cd_data$oldpeak)) + geom_density()

oldpeak <- cd_data$oldpeak
outliers_oldpeak <- boxplot(oldpeak, 
                            main="Depresión del ST (Con descanso) alcanzada por paciente")$out
print(outliers_oldpeak)
```

### Pendiente del segmento ST de ejercicio pico:
```{r}
hist(as.numeric(cd_data$slope), 
     main="Frecuencia pendiente del ST (Con ejercicio pico)")
ggplot(mapping=aes(x = cd_data$slope)) + geom_density()

slope <- cd_data$slope
outliers_slope <- boxplot(slope, 
                          main="Pendiente del ST (Con ejercicio pico) por paciente")$out
print(outliers_slope)
```

### Número de vasos principales (0-3) coloreados por flourosopía:
```{r}
hist(as.numeric(cd_data$ca), 
     main="Frecuencia del número de vasos principales")
ggplot(mapping=aes(x = cd_data$ca)) + geom_density()

ca <- cd_data$ca
outliers_ca <- boxplot(ca, main="Número de vasos principales por paciente")$out
print(outliers_ca)
```

Como se puede observar, claramente, en los gráficos de caja y bigote de presión 
arterial en reposo, depresión del ST inducida por el ejercicio relativo al 
descanso, frecuencia cardíaca máxima alcanzada, suero colestoral en mg/dl y en 
el número de vasos principales (0-3) coloreados por flourosopía, existe la 
presencia de datos "ouliers", los cuales serán eliminados, ya que afectan, 
considerablemente, a la realización del estudio.

### Eliminación de valores "outliers":

```{r}
# Lamentablemente no se pudo realizar la eliminación de todos los datos 
# atípicos de manera correcta, debido a que no se encontró la forma correcta de 
# realizarlo. Sin embargo, ya que era necesario realizarlo, se realizó de una 
# manera muy poco óptima y eficiente (Uno por uno).

outliers_trestbps <- 172  
cd_data$trestbps[cd_data$trestbps == outliers_trestbps] <- NA

outliers_trestbps <- 174
cd_data$trestbps[cd_data$trestbps == outliers_trestbps] <- NA

outliers_trestbps <- 178  
cd_data$trestbps[cd_data$trestbps == outliers_trestbps] <- NA

outliers_trestbps <- 180 
cd_data$trestbps[cd_data$trestbps == outliers_trestbps] <- NA

outliers_trestbps <- 192
cd_data$trestbps[cd_data$trestbps == outliers_trestbps] <- NA

outliers_trestbps <- 200
cd_data$trestbps[cd_data$trestbps == outliers_trestbps] <- NA

# Gráfico de caja y bigote sin valores "ouliers".
boxplot(cd_data$trestbps)

# Puesto que esto ocuría muchas líneas de código es que se optó por no eliminar 
# el resto de los valores "outliers" presentes en el dataset. Sin embargo, para 
# realizarlo, solo basta definir todos los valores outliers (Uno por uno) y, 
# luego, eliminarlos mediante N/A.
```


### Gráficos "plot" para variables Cualitativas y su respectiva descripción:

### Sexo:
```{r}
# Generación gráfico "plot".
plot(cd_data$sex)

# Valores representados en el summary indican lo siguiente:
# 0 <- Femenino.
# 1 <- Masculino.

# Resumen de los datos.
summary(cd_data$sex)
```

### Tipo de dolor en el pecho:
```{r}
plot(cd_data$cp)

# 0 <- Leve.
# 1 <- Moderado.
# 2 <- Fuerte.
# 3 <- Insoportable.

summary(cd_data$cp)
```

### Azúcar en sangre en ayunas > 120 mg/dl:
```{r}
plot(cd_data$fbs)

# 0 <- No es mayor.
# 1 <- Sí es mayor.

summary(cd_data$fbs)
```

### Angina inducida por ejercicio:
```{r}
plot(cd_data$exang)

# 0 <- No Inducida.
# 1 <- Inducida.

summary(cd_data$exang)
```

### Nivel de daño:
```{r}
plot(as.character(cd_data$thal))

# 0 <- Ourlier.
# 1 <- Defecto Reversible.
# 2 <- Defecto Fijo.
# 3 <- Normal.
# 4 <- Outlier.

summary(cd_data$thal)
```

### Ataque cardíaco:
```{r}
plot(cd_data$target)

# 1 <- Ataque Cardíaco.
# 0 <- Sin Ataque Cardíaco.

summary(cd_data$target)
```


## Conclusión:

Al realizar un análisis exhaustivo del dataset, en el cual se realizaron 
diversos procedimientos para poder hacer un análisis lógico de los datos, se 
concluye con que 165 personas han sufrido de un ataque cardíaco frente a 138 que 
no han sufrido de este (Hasta el momento).


### Dataset "final" que contiene todos los datos actualizados:
```{r}
# Dataset "final".
str(cd_data)
```


## Referencias:

1. Chen, M., Zieve, D. (s.f.). Qué es la enfermedad cardiovascular. MedlinePlus. Recuperado de https://medlineplus.gov/spanish/ency/patientinstructions/000759.htm. (Consultado: 07 de agosto 2020).
2. N.N. (s.f.). ¿Qué son las enfermedades cardiovasculares?. Organización Mundial de la Salud. Recuperado de: https://www.who.int/cardiovascular_diseases/about_cvd/es/. (Consutado: 07 de agosto 2020).
3. N.N. (s.f.). Enfermedad cardíaca. Mayo Clinic. Recuperado de https://www.mayoclinic.org/es-es/diseases-conditions/heart-disease/symptoms-causes/syc-20353118. (Consutado: 07 de agosto 2020).













