---
title: "Cardiovascular_Disease_V.2"
author: "Benjamín Rojas Rojas"
date: "12/08/2020"
output:
  pdf_document:
    highlight: zenburn
    toc: yes
    number_sections: true
    toc_depth: 2
    latex_engine: xelatex
  html_document:
    highlight: default
    number_sections: true
    theme: cosmo
    toc: yes
    toc_depth: 2
  word_document: default
---

# Librerias utilizadas:

Las librerias utilizadas para la realización de esta guía fueron las siguientes:

```{r load library, message=FALSE, warning=FALSE}
library(ggplot2)
library(car)
library(gridExtra)
library(grid)
library(plyr)
library(GGally)
```

# Variables "Globales":

```{r}
f <- "fbs"
t <- "target"
h <- "hist"
d <- "density"
b <- "boxplot"
v <- "violin"
```


# Lectura dataset depurado:

```{r read dataset}
# Obtención del dataset.
cardiovascular_disease_V2 <- "data_depurado.csv"

# Lectura del dataset y definición de cd_data ("Cardiovascular Disease Data).
cd_data <- read.csv(cardiovascular_disease_V2, sep = ",")

# Se elimina columna X creada por error.
cd_data$X <- NULL

#str(cd_data)
```

# Cambio del tipo de variable cualitativa:
```{r}
cd_data$sex <- as.factor(cd_data$sex)
cd_data$cp <- as.factor(cd_data$cp)
cd_data$fbs <- as.factor(cd_data$fbs)
cd_data$restecg <- as.factor(cd_data$restecg)
cd_data$exang <- as.factor(cd_data$exang)
cd_data$slope <- as.factor(cd_data$slope)
cd_data$ca <- as.factor(cd_data$ca)
cd_data$thal <- as.factor(cd_data$thal)
cd_data$target <- as.factor(cd_data$target)
#str(cd_data)
```

# Variables cuantitativas expuestas frente a dos variables cualitativas.

El propósito de las comparaciones realizadas es lograr entender el 
comporatamiento de las dos variables cualitativas seleccionadas frente a cada 
una de las variables cuantitativas del dataset. A modo de recuerdo, las 
variables cuantitativas y cualitativas son:

| Cuantitativas | Cualitativas |
|     ---       |      ---     |
|     age       |      sex     |
|   trestbps    |      cp      |
|     chol      |      fbs     |
|   thalach     |    restecg   | 
|   oldpeak     |     exang    |
|               |     slope    |
|               |      ca      |
|               |     thal     |
|               |    target    |


Dicho esto, las variables cualitativas a utilizar son: fbs (Azúcar en sangre en
ayunas > 120 mg/dL [0: < 120; 1: > 120]) y target (Ataque cardíaco [0: Sin 
Ataque Cardíaco; 1: Con Ataque Cardíaco]). Se escogieron estas variables ya que 
se considera que son fundamentales para realizar las comparaciones solicitadas 
y, además, se relacionan muy bien con las variables cuantitativas.

# Función encargada de generar gran parte de los gráficos:
```{r función que crea todos los gráficos}
crea_grafos <- function(columna, titulo, grafo, tipo, legend=FALSE){
  # Leyenda se ubica a la derecha de los gráficos.
  if(legend){
    show_legend = theme(legend.position = "right")
  }
  else{
    show_legend = theme(legend.position = "none")
  }
  
  # Condicionales para darle un valor personalizado a cada gráfico.
  # De esta forma se pueden visualizar de mejor manera.
  if(columna == cd_data$oldpeak){
    # Valor referente a binwidth.
    x = 0.5
    # Valor mínimo del gráfico (Para boxplot y violinplot).
    y_1 = 0
    # Valor máximo del gráfico (Para boxplot y violinplot).
    y_2 = 5
    # Distancia existente entre cada valor (Para boxplot y violinplot).
    y_3 = 0.8
  }
  else if(columna == cd_data$age){
    x = 5
    y_1 = 0
    y_2 = 80
    y_3 = 10
  }
  else if(columna == cd_data$trestbps || columna == cd_data$thalach){
    x = 10
    y_1 = 0
    y_2 = 210
    y_3 = 30
  }
  else if(columna == cd_data$chol){
    x = 20
    y_1 = 0
    y_2 = 380
    y_3 = 40
  }
  
  # Todos los gráficos comparados con la variable "fbs".
  if(tipo == f){
    # Histograma.
    if(grafo == h){
      ggplot(data=cd_data, aes(columna)) +
        geom_histogram(binwidth=x, color="white", aes(fill=fbs)) + 
        xlab(titulo) + 
        ylab("Frecuencia") + 
        show_legend + 
        ggtitle(paste("Histograma de", titulo))
    }
    # Diagrama de Densidad.
    else if(grafo == d){
      ggplot(data=cd_data, aes(columna, color=fbs, fill=fbs)) + 
        geom_density(alpha=.3) +
        xlab(titulo) + 
        ylab("Frecuencia") +
        show_legend +
        ggtitle(paste("Densidad de", titulo))
    }
    # Gráfico Boxplot.
    else if(grafo == b){
      ggplot(data=cd_data, aes(fbs, columna, fill=fbs)) + 
        geom_boxplot() +
        scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
        show_legend
    }
    # Gráfico Violinplot.
    else if(grafo == v){
      ggplot(data=cd_data, aes(fbs, columna, fill=fbs)) + 
        geom_violin(aes(color=fbs), trim = T) +
        scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
        geom_boxplot(width=0.1) +
        show_legend
    }
  }
  
  # Todos los gráficos comparados con la variable "target".
  else if(tipo == t){
    # Histograma.
    if(grafo == h){
      ggplot(data=cd_data, aes(columna)) +
        geom_histogram(binwidth=x, color="white", aes(fill=target)) + 
        xlab(titulo) + 
        ylab("Frecuencia") + 
        show_legend + 
        ggtitle(paste("Histograma de", titulo))
    }
    # Diagrama de Densidad.
    else if(grafo == d){
      ggplot(data=cd_data, aes(columna, color=target, fill=target)) + 
        geom_density(alpha=.3) +
        xlab(titulo) + 
        ylab("Frecuencia") +
        show_legend +
        ggtitle(paste("Densidad de", titulo))
    }
    # Gráfico Boxplot.
    else if(grafo == b){
      ggplot(data=cd_data, aes(target, columna, fill=target)) + 
        geom_boxplot() +
        scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
        show_legend
    }
    # Gráfico Violinplot.
    else if(grafo == v){
      ggplot(data=cd_data, aes(target, columna, fill=target)) + 
        geom_violin(aes(color=target), trim = T) +
        scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
        geom_boxplot(width=0.1) +
        show_legend
    }

  }
    
}

```

## Histogramas:

```{r crea histogramas, message=FALSE, warning=FALSE}
# Generación de 2 números para que se impriman los dos conjuntos de gráficos.
# No se ha logrado modularizar, aún, esta parte y por ello se repite para todos
# los gráficos.
for(i in 1:2){
  if(i == 1){
    # Primer conjunto de histogramas ("fbs").
    age <- crea_grafos(cd_data$age, "Age", h, f)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", h, f)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", h, f)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", h, f)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", h, f, TRUE)
    titulo = "Histogramas fbs"
  }
  
  else if(i == 2){
    # Segundo conjunto de histogramas ("target").
    age <- crea_grafos(cd_data$age, "Age", h, t)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", h, t)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", h, t)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", h, t)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", h, t, TRUE)
    titulo = "Histogramas target"
  }
  # Impresión de ambos histogramas.
  grid.arrange(age + ggtitle(""),
               trestbps + ggtitle(""),
               chol + ggtitle(""),
               thalach + ggtitle(""),
               oldpeak + ggtitle(""),
               nrow = 2,
               top = textGrob(titulo,
                            gp=gpar(frontsize=14))
               )
}
```
Para analizar los gráficos obtenidos los separaremos en aquellos que son para 
la variable "fbs" y para la variable "target".

En primer lugar tenemos los gráficos correspondientes a "fbs", en los cuales se 
puede observar una constante en el comportamiento del gráfico; la gran mayoría 
de los pacientes registrados poseen un nivel de azúcar en ayunas inferior a 120 
mg/dL (0) independiente de la variable con la cual se esté comparando. En 
adición, los pacientes con un nivel de azúcar en ayunas superior a 120 mg/dL, se 
encuentran en la mitad de los datos.

En segundo lugar tenemos los gráficos correspondientes a "target", en los se 
puede observar que una gran cantidad de los pacientes ha sufrido un ataque 
cardíaco (1); observándose así una gran relación entre sufrir un ataque cardíaco 
y las variables comparadas (Edad, Presión arterial en reposo, Suero colestoral 
(En mg/dL), Frecuencia cardíaca máxima alcanzada y Depresión del ST inducida 
por el ejercicio relativo al descanso), ya que, a simple vista, la mitad de cada 
variable cuantitativa de los pacientes, ha sufrido de un ataque cardíaco.

## Diagrama de densidad:

```{r crea gráfico densidad, message=FALSE, warning=FALSE}
for(i in 1:2){
  if(i == 1){
    age <- crea_grafos(cd_data$age, "Age", d, f)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", d, f)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", d, f)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", d, f)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", d, f, TRUE)
    titulo = "Diagramas de densidad de fbs"
  }
  
  else if(i == 2){
    age <- crea_grafos(cd_data$age, "Age", d, t)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", d, t)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", d, t)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", d, t)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", d, t, TRUE)
    titulo = "Diagramas de densidad de target"
  }
  grid.arrange(age + ggtitle(""),
               trestbps + ggtitle(""),
               chol + ggtitle(""),
               thalach + ggtitle(""),
               oldpeak + ggtitle(""),
               nrow = 2,
               top = textGrob(titulo,
                            gp=gpar(frontsize=14))
               )
}
```
Ocurre un caso particular con esta visualización de los datos, ya que difiere de 
la visualización anterior de los datos.

En el caso de la variable "fbs", la gran mayoría de sus valores (0 y 1), se 
encuentran igualados en cantidad en los distintos tipos de variables 
cuantitativas con las que se les está comparando. Por lo que se podría decir que 
hay igual de cantidad de pacientes que poseen niveles mayores y menores a 120 
mg/dL en ayunas.

Para la variable "target" ocurre un caso similar al anterior, producto de que 
los pacientes que han sufrido o no de un ataque cardíaco (1 y 0 repectivamente) 
se encuentran igualados en cuanto a cantidad.

## Gráficos Boxplot:

```{r crea boxplot, message=FALSE, warning=FALSE}
for(i in 1:2){
  if(i == 1){
    age <- crea_grafos(cd_data$age, "Age", b, f)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", b, f)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", b, f)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", b, f)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", b, f, TRUE)
    titulo = "Boxplot de fbs"
  }
  
  else if(i == 2){
    age <- crea_grafos(cd_data$age, "Age", b, t)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", b, t)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", b, t)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", b, t)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", b, t, TRUE)
    titulo = "Boxplot de target"
  }
  grid.arrange(age + ggtitle(""),
               trestbps + ggtitle(""),
               chol + ggtitle(""),
               thalach + ggtitle(""),
               oldpeak + ggtitle(""),
               nrow = 2,
               top = textGrob(titulo,
                            gp=gpar(frontsize=14))
               )
}
```

## Gráficos Violinplot:

```{r crea violinplot, message=FALSE, warning=FALSE}
for(i in 1:2){
  if(i == 1){
    age <- crea_grafos(cd_data$age, "Age", v, f)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", v, f)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", v, f)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", v, f)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", v, f, TRUE)
    titulo = "Violinplot de fbs"
  }
  
  else if(i == 2){
    age <- crea_grafos(cd_data$age, "Age", v, t)
    trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", v, t)
    chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", v, t)
    thalach <- crea_grafos(cd_data$thalach, "Thalach", v, t)
    oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", v, t, TRUE)
    titulo = "Violinplot de target"
  }
  grid.arrange(age + ggtitle(""),
               trestbps + ggtitle(""),
               chol + ggtitle(""),
               thalach + ggtitle(""),
               oldpeak + ggtitle(""),
               nrow = 2,
               top = textGrob(titulo,
                            gp=gpar(frontsize=14))
               )
}
```
En los gráficos generados para la variable "fbs" se puede observar una tendencia 
a azúcar en sangre en ayunas < 120 mg/dL en las variables "age" y "trestbps". En 
cuanto al resto de las variables no se puede observar una tendencia clara de los 
datos ya que los gráficos son demasiado parecidos entre sí.

Respecto a los gráficos generados para la variable "target", no se logra 
observar ninguna variable como tal que tenga una tendencia clara de los datos, 
producto de que los gráficos generados son muy parecidos entre sí. De todos 
modos, lo que se puede observar es una tendencia de un ataque ardíaco cuando los 
valores de "trestbps" se encuentran entre 120 - 130 (Aproximadamente), "target" 
entre 200 - 240 y "thalach" se encuentran entre 150 - 180.

## Visualización de datos con scatterplot:

```{r scatterplot, message=FALSE, warning=FALSE}

ggplot(data=cd_data, aes(x=age, y=trestbps)) +
  geom_point(aes(color=fbs, shape=fbs)) +
  geom_smooth(method = "lm") +
  ggtitle("Age v/s Trestbps")

# No se entiende del todo lo que se solicita.
```

Visualice los datos utilizando un scatterplot y compare básicamente contra ambas variables categóricas o cualitativas.

### Visualización basada en una regresión lineal:


Del punto anterior, agregue una visualización basada en una regresión lineal.

### Análisis de correlación:

```{r correlación, message=FALSE, warning=FALSE}
# Creación de un "subconjunto" de los datos para trabajar solo con las variables
# cuantitativas del cd_data.
col <- c("age", "trestbps", "chol", "thalach", "oldpeak")
quantitative <- cd_data[ , (names(cd_data) %in% col)]

# Generación del gráfico de las correlaciones.
ggpairs(data=quantitative[1: 5], 
title = "Correlation Cardiovascular Disease",
upper = list(continuous = wrap("cor", size=5)),
lower = list(continuous = "smooth"))
```
Como se puede observar en el gráfico, las relaciones observadas no superan el 
30% de correlación, siendo las mayores, las correlaciones entre las variables 
"trestbps" y "age" con un 27,4% y "oldpeak" y "age", con un 20,4% de 
correlación.
Con esto se podría afirmar que no existe mucha correlación entre las variables 
cuantitativas.

## Heatmap de los datos:

No se ha logrado, de momento, desarrollar el "heatmap" con éxito :c.
