show_legend +
ggtitle(paste("Densidad de", titulo))
}
# Gráfico Boxplot.
else if(grafo == b){
ggplot(data=cd_data, aes(target, columna, fill=target)) +
geom_boxplot() +
scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
show_legend
}
# Gráfico Violinplot.
else if(grafo == v){
ggplot(data=cd_data, aes(target, columna, fill=target)) +
geom_violin(aes(color=target), trim = T) +
scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
geom_boxplot(width=0.1) +
show_legend
}
}
}
crea_scatter <- function(columna1, columna2, nombre_x, nombre_y,
tipo, legend=FALSE){
# Leyenda se ubica a la derecha de los gráficos.
if(legend){
show_legend = theme(legend.position = "right")
}
else{
show_legend = theme(legend.position = "none")
}
# Scatterplots para la variable "fbs".
if(tipo == f){
ggplot(data=cd_data, aes(x=columna1, y=columna2)) +
geom_point(aes(color=fbs, shape=fbs)) +
xlab(nombre_x) +
ylab(nombre_y) +
geom_smooth(method = "lm") +
ggtitle(titulo) +
show_legend
}
# Scatterplots para la variable "target".
else if(tipo == t){
ggplot(data=cd_data, aes(x=columna1, y=columna2)) +
geom_point(aes(color=target, shape=t)) +
xlab(nombre_x) +
ylab(nombre_y) +
geom_smooth(method = "lm") +
ggtitle(titulo) +
show_legend
}
}
# Generación de 2 números para que se impriman los dos conjuntos de gráficos.
# No se ha logrado modularizar, aún, esta parte y por ello se repite para todos
# los gráficos.
for(i in 1:2){
if(i == 1){
# tipo toma los valores de "f" y "t" para formar los distintos tipos de
# gráficos que serán categorizados en "fbs" y "target" respectivamente.
tipo <- f
# Título personalizado para cada conjunto de gráficos.
titulo <- "Histogramas de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Histogramas de target"
}
# Parámetros de ambos histogramas.
age <- crea_grafos(cd_data$age, "Age", h, tipo)
trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", h, tipo)
chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", h, tipo)
thalach <- crea_grafos(cd_data$thalach, "Thalach", h, tipo)
oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", h, tipo, TRUE)
# Impresión de ambos histogramas.
grid.arrange(age + ggtitle(""),
trestbps + ggtitle(""),
chol + ggtitle(""),
thalach + ggtitle(""),
oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
for(i in 1:2){
if(i == 1){
tipo <- f
titulo <- "Diagramas de densidad de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Diagramas de densidad de target"
}
age <- crea_grafos(cd_data$age, "Age", d, tipo)
trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", d, tipo)
chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", d, tipo)
thalach <- crea_grafos(cd_data$thalach, "Thalach", d, tipo)
oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", d, tipo, TRUE)
grid.arrange(age + ggtitle(""),
trestbps + ggtitle(""),
chol + ggtitle(""),
thalach + ggtitle(""),
oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
for(i in 1:2){
if(i == 1){
tipo <- f
titulo <- "Boxplots de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Boxplots de target"
}
age <- crea_grafos(cd_data$age, "Age", b, tipo)
trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", b, tipo)
chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", b, tipo)
thalach <- crea_grafos(cd_data$thalach, "Thalach", b, tipo)
oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", b, tipo, TRUE)
grid.arrange(age + ggtitle(""),
trestbps + ggtitle(""),
chol + ggtitle(""),
thalach + ggtitle(""),
oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
for(i in 1:2){
if(i == 1){
tipo <- f
titulo <- "Violinplots de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Violinplots de target"
}
age <- crea_grafos(cd_data$age, "Age", v, tipo)
trestbps <- crea_grafos(cd_data$trestbps, "Trestbps", v, tipo)
chol <- crea_grafos(cd_data$chol, "Chol (mg/dL)", v, tipo)
thalach <- crea_grafos(cd_data$thalach, "Thalach", v, tipo)
oldpeak <- crea_grafos(cd_data$oldpeak, "Oldpeak", v, tipo, TRUE)
grid.arrange(age + ggtitle(""),
trestbps + ggtitle(""),
chol + ggtitle(""),
thalach + ggtitle(""),
oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
for(i in 1:2){
if(i == 1){
tipo <- f
titulo <- "Scatterplots de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Scatterplots de target"
}
# Todas las comparaciones para age.
age_v_trestbps <- crea_scatter(cd_data$age, cd_data$trestbps,
"Age", "Trestbps", tipo)
age_v_chol <- crea_scatter(cd_data$age, cd_data$chol,
"Age", "Chol", tipo)
age_v_thalach <- crea_scatter(cd_data$age, cd_data$thalach,
"Age", "Thalach", tipo)
age_v_oldpeak <- crea_scatter(cd_data$age, cd_data$oldpeak,
"Age", "Oldpeak", tipo)
# Todas las comparaciones restantes para trestbps.
trestbps_v_chol <- crea_scatter(cd_data$trestbps, cd_data$chol,
"Trestbps", "Chol", tipo)
trestbps_v_thalach <- crea_scatter(cd_data$trestbps, cd_data$thalach,
"Trestbps", "Thalach", tipo)
trestbps_v_oldpeak <- crea_scatter(cd_data$trestbps, cd_data$oldpeak,
"Trestbps", "Oldpeak", tipo)
# Todas las comparaciones restantes para chol.
chol_v_thalach <- crea_scatter(cd_data$chol, cd_data$thalach,
"Chol", "Thalach", tipo)
chol_v_oldpeak <- crea_scatter(cd_data$chol, cd_data$oldpeak,
"Chol", "Oldpeak", tipo)
# Todas las comparaciones restantes para thalach.
thalach_v_oldpeak <- crea_scatter(cd_data$thalach, cd_data$oldpeak,
"Thalach", "Oldpeak", tipo, TRUE)
# Impresión de todos los gráficos Scatterplot.
grid.arrange(age_v_trestbps + ggtitle(""),
age_v_chol + ggtitle(""),
age_v_thalach + ggtitle(""),
age_v_oldpeak + ggtitle(""),
trestbps_v_chol + ggtitle(""),
trestbps_v_thalach + ggtitle(""),
trestbps_v_oldpeak + ggtitle(""),
chol_v_thalach + ggtitle(""),
chol_v_oldpeak + ggtitle(""),
thalach_v_oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
"
age_v_trestbps <- scatterplot(cd_data$age, cd_data$trestbps)
age_v_chol <- scatterplot(cd_data$age, cd_data$chol)
age_v_thalach <- scatterplot(cd_data$age, cd_data$thalach)
age_v_oldpeak <- scatterplot(cd_data$age, cd_data$oldpeak)
trestbps_v_chol <- scatterplot(cd_data$trestbps, cd_data$chol)
trestbps_v_thalach <- scatterplot(cd_data$trestbps, cd_data$thalach)
trestbps_v_oldpeak <- scatterplot(cd_data$trestbps, cd_data$oldpeak)
chol_v_thalach <- scatterplot(cd_data$chol, cd_data$thalach)
chol_v_oldpeak <- scatterplot(cd_data$chol, cd_data$oldpeak)
thalach_v_oldpeak <- scatterplot(cd_data$thalach, cd_data$oldpeak)
grid.arrange(age_v_trestbps, age_v_chol,
age_v_thalach,
age_v_oldpeak,
trestbps_v_chol,
trestbps_v_thalach,
trestbps_v_oldpeak,
chol_v_thalach,
chol_v_oldpeak,
thalach_v_oldpeak,
nrow = 1
)
"
# Creación de un "subconjunto" de los datos para trabajar solo con las variables
# cuantitativas del cd_data.
col <- c("age", "trestbps", "chol", "thalach", "oldpeak")
quantitative <- cd_data[ , (names(cd_data) %in% col)]
# Generación del gráfico de las correlaciones.
ggpairs(data=quantitative[1: 5],
title = "Correlation Cardiovascular Disease",
upper = list(continuous = wrap("cor", size=5)),
lower = list(continuous = "smooth"))
#age_v_trestbps <- scatterplot(cd_data$age, cd_data$trestbps)
#age_v_chol <- scatterplot(cd_data$age, cd_data$chol)
#age_v_thalach <- scatterplot(cd_data$age, cd_data$thalach)
#age_v_oldpeak <- scatterplot(cd_data$age, cd_data$oldpeak)
#trestbps_v_chol <- scatterplot(cd_data$trestbps, cd_data$chol)
#trestbps_v_thalach <- scatterplot(cd_data$trestbps, cd_data$thalach)
#trestbps_v_oldpeak <- scatterplot(cd_data$trestbps, cd_data$oldpeak)
#chol_v_thalach <- scatterplot(cd_data$chol, cd_data$thalach)
#chol_v_oldpeak <- scatterplot(cd_data$chol, cd_data$oldpeak)
#thalach_v_oldpeak <- scatterplot(cd_data$thalach, cd_data$oldpeak)
#grid.arrange(age_v_trestbps, age_v_chol,
#age_v_thalach,
#age_v_oldpeak,
#trestbps_v_chol,
#trestbps_v_thalach,
#trestbps_v_oldpeak,
#chol_v_thalach,
#chol_v_oldpeak,
#thalach_v_oldpeak,
#nrow = 1
#)
# Regresiones lineales para todos los "age v/s ...".
scatterplot(cd_data$age, cd_data$trestbps)
scatterplot(cd_data$age, cd_data$chol)
scatterplot(cd_data$age, cd_data$thalach)
scatterplot(cd_data$age, cd_data$oldpeak)
# Regresiones lineales para los "trestbps v/s ..." restantes.
scatterplot(cd_data$trestbps, cd_data$chol)
scatterplot(cd_data$trestbps, cd_data$thalach)
scatterplot(cd_data$trestbps, cd_data$oldpeak)
# Regresiones lineales para los "chol v/s ..." restantes.
scatterplot(cd_data$chol, cd_data$thalach)
scatterplot(cd_data$chol, cd_data$oldpeak)
# Regresiones lineales para los "thalach v/s ..." restantes.
scatterplot(cd_data$thalach, cd_data$oldpeak)
for(i in 1:2){
if(i == 1){
tipo <- f
titulo <- "Scatterplots de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Scatterplots de target"
}
# Todas las comparaciones para age.
age_v_trestbps <- crea_scatter(cd_data$age, cd_data$trestbps,
"Age", "Trestbps", tipo)
age_v_chol <- crea_scatter(cd_data$age, cd_data$chol,
"Age", "Chol", tipo)
age_v_thalach <- crea_scatter(cd_data$age, cd_data$thalach,
"Age", "Thalach", tipo)
age_v_oldpeak <- crea_scatter(cd_data$age, cd_data$oldpeak,
"Age", "Oldpeak", tipo)
# Todas las comparaciones restantes para trestbps.
trestbps_v_chol <- crea_scatter(cd_data$trestbps, cd_data$chol,
"Trestbps", "Chol", tipo)
trestbps_v_thalach <- crea_scatter(cd_data$trestbps, cd_data$thalach,
"Trestbps", "Thalach", tipo)
trestbps_v_oldpeak <- crea_scatter(cd_data$trestbps, cd_data$oldpeak,
"Trestbps", "Oldpeak", tipo)
# Todas las comparaciones restantes para chol.
chol_v_thalach <- crea_scatter(cd_data$chol, cd_data$thalach,
"Chol", "Thalach", tipo)
chol_v_oldpeak <- crea_scatter(cd_data$chol, cd_data$oldpeak,
"Chol", "Oldpeak", tipo)
# Todas las comparaciones restantes para thalach.
thalach_v_oldpeak <- crea_scatter(cd_data$thalach, cd_data$oldpeak,
"Thalach", "Oldpeak", tipo, TRUE)
# Impresión de todos los gráficos Scatterplot.
grid.arrange(age_v_trestbps + ggtitle(""),
age_v_chol + ggtitle(""),
age_v_thalach + ggtitle(""),
age_v_oldpeak + ggtitle(""),
trestbps_v_chol + ggtitle(""),
trestbps_v_thalach + ggtitle(""),
trestbps_v_oldpeak + ggtitle(""),
chol_v_thalach + ggtitle(""),
chol_v_oldpeak + ggtitle(""),
thalach_v_oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
library(ggplot2)
library(car)
library(gridExtra)
library(grid)
library(plyr)
library(GGally)
# Esto para comodidad a la hora de querer variar parámetros con mayor rapidez.
f <- "fbs"
t <- "target"
h <- "hist"
d <- "density"
b <- "boxplot"
v <- "violin"
#s <- "scatter" # No funciona la idea de crear gráficos "s" y "r" dentro
# de la misma función.
#r <- "regretion"
# Obtención del dataset.
cardiovascular_disease_V2 <- "data_depurado.csv"
# Lectura del dataset y definición de cd_data ("Cardiovascular Disease Data).
cd_data <- read.csv(cardiovascular_disease_V2, sep = ",")
# Se elimina columna X creada por error.
cd_data$X <- NULL
#str(cd_data)
cd_data$sex <- as.factor(cd_data$sex)
cd_data$cp <- as.factor(cd_data$cp)
cd_data$fbs <- as.factor(cd_data$fbs)
cd_data$restecg <- as.factor(cd_data$restecg)
cd_data$exang <- as.factor(cd_data$exang)
cd_data$slope <- as.factor(cd_data$slope)
cd_data$ca <- as.factor(cd_data$ca)
cd_data$thal <- as.factor(cd_data$thal)
cd_data$target <- as.factor(cd_data$target)
#str(cd_data)
crea_grafos <- function(columna, titulo, grafo, tipo, legend=FALSE){
# Leyenda se ubica a la derecha de los gráficos.
if(legend){
show_legend = theme(legend.position = "right")
}
else{
show_legend = theme(legend.position = "none")
}
# Condicionales para darle un valor personalizado a cada gráfico.
# De esta forma se pueden visualizar de mejor manera.
if(columna == cd_data$oldpeak){
# Valor referente a binwidth.
x = 0.5
# Valor mínimo del gráfico (Para boxplot y violinplot).
y_1 = 0
# Valor máximo del gráfico (Para boxplot y violinplot).
y_2 = 5
# Distancia existente entre cada valor (Para boxplot y violinplot).
y_3 = 0.8
}
else if(columna == cd_data$age){
x = 5
y_1 = 0
y_2 = 80
y_3 = 10
}
else if(columna == cd_data$trestbps || columna == cd_data$thalach){
x = 10
y_1 = 0
y_2 = 210
y_3 = 30
}
else if(columna == cd_data$chol){
x = 20
y_1 = 0
y_2 = 380
y_3 = 40
}
# Todos los gráficos comparados con la variable "fbs".
if(tipo == f){
# Histograma.
if(grafo == h){
ggplot(data=cd_data, aes(columna)) +
geom_histogram(binwidth=x, color="white", aes(fill=fbs)) +
xlab(titulo) +
ylab("Frecuencia") +
show_legend +
ggtitle(paste("Histograma de", titulo))
}
# Diagrama de Densidad.
else if(grafo == d){
ggplot(data=cd_data, aes(columna, color=fbs, fill=fbs)) +
geom_density(alpha=.3) +
xlab(titulo) +
ylab("Frecuencia") +
show_legend +
ggtitle(paste("Densidad de", titulo))
}
# Gráfico Boxplot.
else if(grafo == b){
ggplot(data=cd_data, aes(fbs, columna, fill=fbs)) +
geom_boxplot() +
scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
show_legend
}
# Gráfico Violinplot.
else if(grafo == v){
ggplot(data=cd_data, aes(fbs, columna, fill=fbs)) +
geom_violin(aes(color=fbs), trim = T) +
scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
geom_boxplot(width=0.1) +
show_legend
}
}
# Todos los gráficos comparados con la variable "target".
else if(tipo == t){
# Histograma.
if(grafo == h){
ggplot(data=cd_data, aes(columna)) +
geom_histogram(binwidth=x, color="white", aes(fill=target)) +
xlab(titulo) +
ylab("Frecuencia") +
show_legend +
ggtitle(paste("Histograma de", titulo))
}
# Diagrama de Densidad.
else if(grafo == d){
ggplot(data=cd_data, aes(columna, color=target, fill=target)) +
geom_density(alpha=.3) +
xlab(titulo) +
ylab("Frecuencia") +
show_legend +
ggtitle(paste("Densidad de", titulo))
}
# Gráfico Boxplot.
else if(grafo == b){
ggplot(data=cd_data, aes(target, columna, fill=target)) +
geom_boxplot() +
scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
show_legend
}
# Gráfico Violinplot.
else if(grafo == v){
ggplot(data=cd_data, aes(target, columna, fill=target)) +
geom_violin(aes(color=target), trim = T) +
scale_y_continuous(titulo, breaks=seq(y_1, y_2, by=y_3)) +
geom_boxplot(width=0.1) +
show_legend
}
}
}
for(i in 1:2){
if(i == 1){
tipo <- f
titulo <- "Scatterplots de fbs"
}
else if(i == 2){
tipo <- t
titulo <- "Scatterplots de target"
}
# Todas las comparaciones para age.
age_v_trestbps <- crea_scatter(cd_data$age, cd_data$trestbps,
"Age", "Trestbps", tipo)
age_v_chol <- crea_scatter(cd_data$age, cd_data$chol,
"Age", "Chol", tipo)
age_v_thalach <- crea_scatter(cd_data$age, cd_data$thalach,
"Age", "Thalach", tipo)
age_v_oldpeak <- crea_scatter(cd_data$age, cd_data$oldpeak,
"Age", "Oldpeak", tipo)
# Todas las comparaciones restantes para trestbps.
trestbps_v_chol <- crea_scatter(cd_data$trestbps, cd_data$chol,
"Trestbps", "Chol", tipo)
trestbps_v_thalach <- crea_scatter(cd_data$trestbps, cd_data$thalach,
"Trestbps", "Thalach", tipo)
trestbps_v_oldpeak <- crea_scatter(cd_data$trestbps, cd_data$oldpeak,
"Trestbps", "Oldpeak", tipo)
# Todas las comparaciones restantes para chol.
chol_v_thalach <- crea_scatter(cd_data$chol, cd_data$thalach,
"Chol", "Thalach", tipo)
chol_v_oldpeak <- crea_scatter(cd_data$chol, cd_data$oldpeak,
"Chol", "Oldpeak", tipo)
# Todas las comparaciones restantes para thalach.
thalach_v_oldpeak <- crea_scatter(cd_data$thalach, cd_data$oldpeak,
"Thalach", "Oldpeak", tipo, TRUE)
# Impresión de todos los gráficos Scatterplot.
grid.arrange(age_v_trestbps + ggtitle(""),
age_v_chol + ggtitle(""),
age_v_thalach + ggtitle(""),
age_v_oldpeak + ggtitle(""),
trestbps_v_chol + ggtitle(""),
trestbps_v_thalach + ggtitle(""),
trestbps_v_oldpeak + ggtitle(""),
chol_v_thalach + ggtitle(""),
chol_v_oldpeak + ggtitle(""),
thalach_v_oldpeak + ggtitle(""),
nrow = 2,
top = textGrob(titulo,
gp=gpar(frontsize=14))
)
}
