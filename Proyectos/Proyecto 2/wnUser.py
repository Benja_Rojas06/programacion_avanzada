#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk


# Clase para crear un usuario.
class Usuario():


    # Constructor
    def __init__(self,object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("Proyecto2.ui")

        self.creadores = self.builder.get_object(object_name)
        self.nombre = self.builder.get_object("entryNombre")
