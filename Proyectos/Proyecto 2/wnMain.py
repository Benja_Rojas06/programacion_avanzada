#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from wnAbout import About
from wnImagenProte import Imagen
from wnUser import Usuario


# Clase principal de la cual sale "todo".
class Principal():


    # Constructor.
    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('Proyecto2.ui')

        main_window = self.builder.get_object('wnMain')
        # Tamaño escogido para la ventana principal.
        main_window.resize(800,600)
        # Título de la ventana principal.
        main_window.set_title("Visualizador de Proteinas")
        main_window.connect('destroy', Gtk.main_quit)

        # Creación Boton About.
        boton_about = self.builder.get_object('btnAbout')
        boton_about.connect('clicked', self.boton_about_clicked)

        # Creación Botón que crea la imagen de la proteína.
        boton_imagen = self.builder.get_object('btnOpen')
        boton_imagen.connect('clicked', self.boton_imagen_clicked)

        # Creación Botón de usuario
        boton_usuario = self.builder.get_object('btnUsuario')
        boton_usuario.connect("clicked", self.boton_usuario_clicked)

        self.labelUsuario = self.builder.get_object("labelUsuario")
        # Muestra la pantalla.
        main_window.show_all()


    # Creación de las funciones para los respectivos botones.
    def boton_about_clicked(self, btn=None):

        ventanaAbout = About(object_name='wnAbout')
        ventanaAbout.acerca_de.run()
        ventanaAbout.acerca_de.destroy()


    def boton_imagen_clicked(self, btn=None):

        ventanaImagen = Imagen(object_name='wnFileChooser')
        ventanaImagen.image.run()
        ventanaImagen.image.destroy()


    def boton_usuario_clicked(self, btn=None):

        ventanaUsuario = Usuario(object_name='wnUser')
        btn_signal = ventanaUsuario.creadores.run()

        if btn_signal == Gtk.ResponseType.OK:
            usuario = ventanaUsuario.nombre.get_text()
            usuario_upper = usuario.upper()
            self.labelUsuario.set_text(usuario_upper)

        ventanaUsuario.creadores.destroy()


# Función Main
if __name__ == '__main__':

    Principal()
    Gtk.main()
