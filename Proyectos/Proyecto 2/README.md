# Visualizador de Proteínas:

Created by: Sebastián Tapia & Benjamín Rojas.

Docente: Fabio Durán.

Módulo: Programación Avanzada.


-> Acerca de la aplicación:

Consiste en un "Visualizador de Proteínas" que crea las imágenes de las
proteínas a partir de su archivo descargado de "PDB", esto gracias a pymol.
Además, crea los archivos .ATOM, .HETATM, .ANISOU y .OTHERS gracias a la
utilización de biopandas.


-> Funcionamiento e Interacción:

El programa comienza con la apertura de la ventana principal en la que se dan 3
opciones, una para saber acerca de los desarrolladores del programa (wnAbout),
otra para crear los archivos (wnFileChooser) ATOM, HETATM, ANISOU, OTHERS
y, además, la imagen de la proteína y otra para crear un usuario (wnUser). Cabe destacar que cada ventana (wnAbout, wnFileChooser y wnUser) poseen botones que permiten interactuar en ella. En el caso de wnAbout el botón "OK" visualizado en
la parte inferior permitirá el cierre de la pestaña. Por su parte, wnFileChooser
un filtro gracias al cual solo se visualizarán los archivos ".pbd" (Debido a que
solo se trabaja con ellos para la formación de las imágenes y de los formatos) y
dos botones, uno para aceptar el archivo (Con este se procederá a crear todo lo mencionado con anterioridad) y otro para cancelar, con el cual se cerrará la
ventana. Por último, wnUser posee una entrada de texto a través de la cual, se
solicitará un nombre de usuario a quien esté usando el programa (Netamente para
tener más cercanía con el usuario) y dos botones, uno para aplicar el nombre de
usuario y otro para cancelar.


-> Fallos que posee el programa:

No permite la visualización de la proteína desde la ventana gráfica, puesto que
al momento de crearla, el programa se cierra.

No se permite la visulización/lectura de la información referente a la proteína
debido a que el programa, una vez crea los archivos, se cierra.

El programa crea la imagen y la información solicitada respecto del archivo
pdb, mas no la abre ni se permite acceder a los formatos creados directamente
desde la aplicación gráfica (Tampoco guarda la información). Solo las muestra
a través de la terminal.


-> Puntos a considerar:

Los archivos en los formatos creados se guardan en la carpeta "Proteinas" y las
imágenes en la carpeta "Proyecto 2" (No confundir las imágenes creadas con la
imagen de la proteína 6uri, puesto que esta se utiliza para "decorar" wnAbout).

-> Aspectos incluidos de la PEP 8:

Solo se utilizaron espacios, para no crear una inconsistencia en el programa
con la utilización de tabulaciones (4 espacios).

Se respeta los 2 espacios que deben existir entre funciones y clases.

Los comentarios se escribieron sobre cada línea de código con el fin de lograr
explicarlo de una manera más adecuada.

No existen lineas de código que superen los 79 caracteres.

No hay comentarios que superen las 72 lineas de caracteres.
