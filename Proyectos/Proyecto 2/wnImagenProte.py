#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import pymol
from biopandas.pdb import PandasPdb


# Clase que crea el archivo con la imagen de la proteína.
class Imagen():


    # Constructor
    def __init__(self, object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('Proyecto2.ui')

        self.image = self.builder.get_object(object_name)

        # Creación de filtro que hará que solo se puedan visualizar
        # archivos en formato ".pdb"
        filtroProte = Gtk.FileFilter()
        filtroProte.set_name('Proteínas Válidas (.pdb)')
        filtroProte.add_pattern('*.pdb')
        self.image.add_filter(filtroProte)

        botonAceptar = self.builder.get_object("btnAccept")
        botonAceptar.connect('clicked', self.datos_PDB)

        # Definición del botón que se encargará de cerrar la ventana.
        self.btn_cancel = self.builder.get_object('btnCancel')
        self.btn_cancel.connect('clicked', self.cancel_clicked)


    # Función que trabaja con nuestro archivo PDB
    # para mostrar su información en los formatos solicitados
    # (ATOM, HETATM, ANISOU, OTHERS) y, además, para la creación
    # de la imagen.
    def datos_PDB(self, btn=None):

        self.proteina = self.image.get_file()
        self.nombreProteina = self.proteina.get_basename()
        print(self.nombreProteina)

        self.rute = self.image.get_current_folder()
        print(self.rute)

        # Información PDB para la creación de los archivos con
        # los formatos previamente mencionados.
        # Código reutilizado y adaptado para lo solicitado.
        ppdb = PandasPdb()
        ppdb.read_pdb(self.rute + "/" + self.nombreProteina)
        for i in ppdb.df.keys():
            print (i)
            print(ppdb.df[i])
            print("\n")

            # formato = "ATOM, HETATM, ANISOU, OTHERS".
            formato = self.nombreProteina + "_" + i + ".pdb"
            ppdb.to_pdb(path = (self.rute + "/" + formato),
                        records = [i],
                        gz = False,
                        append_newline = True)

        # Creación de la imagen PBD.
        # Código extraído de ejemplo del profesor.
        pymol.cmd.load(self.rute + "/" + self.nombreProteina,
                                         self.nombreProteina)
        pymol.cmd.disable("all")
        pymol.cmd.enable(self.nombreProteina)
        print(pymol.cmd.get_names())
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(self.nombreProteina)
        pymol.cmd.png("%s.png" % (self.nombreProteina))
        pymol.cmd.quit()


    # Cierra la ventana en caso de presionar botón 'Cancel'.
    def cancel_clicked(self, btn=None):

        self.image.destroy()
