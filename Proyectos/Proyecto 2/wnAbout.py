#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Clase con la información acerca del nombre del proyecto,
# de los creadores y del docente a cargo. Además, posee la
# imagen de la proteína "6uri".
class About():


    # Constructor
    def __init__(self, object_name = ""):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('Proyecto2.ui')

        self.acerca_de = self.builder.get_object(object_name)
        self.acerca_de.show_all()

        # Definición del botón que se encargará de cerrar la ventana.
        self.cerrar_ok = self.builder.get_object('btnOK')
        self.cerrar_ok.connect('clicked', self.ok_clicked)


    # Cierra la ventana en caso de presionar botón 'OK'.
    def ok_clicked(self, btn=None):

        self.acerca_de.destroy()
