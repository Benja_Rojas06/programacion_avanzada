#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Usuario:


    # Constructor.
    def __init__(self,usuario):
        self._usuario = usuario


    # Método get.
    def getName(self):
        return self._usuario


    # Método set.
    def setName(self, critica_usuario):
        if isinstance(critica_usuario, str):
            self._usuario = critica_usuario
