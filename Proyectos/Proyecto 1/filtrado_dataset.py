#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd


def filter_process():

    # Se realiza la lectura del 'csv' original.
    dataset = pd.read_csv("movies.csv")

    # Filtro del dataset en cual se seleccionarán las columnas
    # con las que se trabajará.
    new_dataset = dataset.filter(["title", "genre", "year",
                                  "director", "reviews_from_users",
                                  "reviews_from_critics"])
    # Filtro para que las películas solo sean del año 2015 en adelante.
    filter_year_dataset = new_dataset[new_dataset['year'] >= 2015]
    # Selección de los primeras 200 películas para
    # reducir el tamaño del dataset.
    new_movies_dataset = filter_year_dataset[0:151]
    # Se ordena la columna 'movies' por año ascendente.
    new_movies_dataset_filter = new_movies_dataset.sort_values(by='year')
    # Creación del nuevo archivo 'csv'.
    new_movies_dataset_filter.to_csv("new_movies.csv", sep = ";", index=False)


if __name__ == '__main__':
    filter_process()
