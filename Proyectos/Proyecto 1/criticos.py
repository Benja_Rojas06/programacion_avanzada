#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from usuarios import Usuario


# Clase Critico creada usando los atributos de clase Usuario.
class Critico(Usuario):
    pass
