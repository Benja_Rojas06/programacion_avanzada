#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from titulo import Titulo
from director import Director
from year import Year
from generos import Generos
from usuarios import Usuario
from criticos import Critico
import pandas as pd
import time
import os


# Función para borrar pantalla.
def clear_screen():
    os.system('cls' if os.name=='nt' else 'clear')


# Función encargada de leer el archivo 'csv'.
def process_file():

    data = pd.read_csv("new_movies.csv", sep=";")

    return data


# Función encargada de agregar movies.
def main_peliculas(data):

    peliculas = []

    # Recorrido del csv.
    for indice in data:
        # Validación de la clase con variable.
        t_movie = Titulo()
        # Instancia de la clase.
        t_movie.setNombre(indice)
        # Agregamos movies a la lista.
        peliculas.append(t_movie)

    return peliculas


# Función encargada de agregar directores, años, ptj usuarios y críticos.
def main_de(data, movies, mostrar):

    # Recorrido de la matriz del csv.
    for indice, fila in data.iterrows():
        # Obtención de datos tipo pelicula[columna].
        pelicula = fila['title']

        # Condicionales para la obtención de los distintos tipos de columnas.
        if(mostrar.upper() == "D"):
            # Obtención de datos tipo director[columna].
            director = fila["director"]
            # Instancia de la clase.
            main_director = Director(director)
            # Recorrido del subdataset.
            for elemento in movies:
                # Si hay coincidencia del nombre agregado con la variable
                # de tipo película procederá a asociar director con película.
                if elemento.getNombre() == pelicula:
                    elemento.setDirector(main_director)

        elif(mostrar.upper() == "A"):
            # Obtención de datos tipo año[columna].
            year = fila['year']
            # Instancia de la clase.
            main_year = Year(year)
            # Recorrido del subdataset.
            for anio in movies:
                # Si hay coincidencia del nombre agregado con la variable
                # de tipo película procederá a asociar año con película.
                if anio.getNombre() == pelicula:
                    anio.setAnio(main_year)

        elif(mostrar.upper() == "U"):
            # Obtención de datos tipo opinión_usario[columna].
            opinion_u = fila['reviews_from_users']
            # Instancia de la clase.
            main_usuario = Usuario(opinion_u)
            # Recorrido del subdataset.
            for usuarios in movies:
                # Si hay coincidencia del nombre agregado con la variable de
                # tipo película procederá a asociar ptj_usario con película.
                if usuarios.getNombre() == pelicula:
                    usuarios.setUsuario(main_usuario)

        elif(mostrar.upper() == "C"):
            # Obtención de datos tipo opinión_crítico[columna].
            opinion_c = fila['reviews_from_critics']
            # Instancia de la clase.
            main_critico = Critico(opinion_c)
            # Recorrido del subdataset.
            for criticos in movies:
                # Si hay coincidencia del nombre agregado con la variable de
                # tipo película procederá a asociar ptj_crítico con película.
                if criticos.getNombre() == pelicula:
                    criticos.setCritico(main_critico)

        else:
            listar_pelicula_y(movie, data_movie, mostrar)

    return movies


# Función que se encarga de eliminar elementos repetidos, ya que
# no se pudo encontrar el error de su copia.
def eliminar_repetidos(elementos):

    elementos_repetidos = []
    # En esta lista se almacenarán los no repetidos.
    lista_filtrada = []
    # Método para eliminar elementos repetidos.
    filtrado = filter(lambda x : x not in lista_filtrada, elementos)
    # Se agregarán solo los elementos no repetidos.
    lista_filtrada.extend(filtrado)

    # Se retorna la lista ya filtrada.
    return lista_filtrada


# Función netamente creada para ahorrar lineas de código.
def borrar():
    time.sleep(.6)
    clear_screen()


# Función netamente creada para ahorrar lineas de código.
def eleccion(opc):

    if(opc.upper() == "M"):
        menu()

    else:
        borrar()
        print("\nMuchas gracias por preferirnos!...")
        print("No seríamos nada sin usted!!!\n")
        exit()


# Función que lista las películas y el filtro seleccionado.
def listar_pelicula_y(movies, data_movie):

    borrar()
    print(" \tPelículas y... \n")
    print(" D: Directores")
    print(" A: Año")
    print(" C: Puntaje Críticos")
    print(" U: Puntaje Usuarios\n")
    mostrar = str(input(" Ingrese lo que desea visualizar: "))
    borrar()
    # Condicionales para mostrar películas y lo que el
    # usuario haya seleccionado.
    if(mostrar.upper() == "D"):
        movies = main_de(data_movie, movies, mostrar)
        # Recorre el dataset de movies.
        for titulo in movies:
            # Recorremos los atributos que hay en clase Titulo.
            for director in titulo.getDirector():
                print(" Director: {} || "
                      " Película: {}".format(director.getNombre(),
                                             titulo.getNombre()))

    elif(mostrar.upper() == "A"):
        movies = main_de(data_movie, movies, mostrar)
        # Recorre el dataset de movies.
        for titulo in movies:
            # Recorremos los atributos que hay en clase Titulo.
            for year in titulo.getAnio():
                print(" Película: {} || "
                      " Año: {}".format(titulo.getNombre(),
                                        year.getYear()))

    elif(mostrar.upper() == "U"):
        movies = main_de(data_movie, movies, mostrar)
        # Recorre el dataset de movies.
        for titulo in movies:
            # Recorremos los atributos que hay en clase Titulo.
            for usuario in titulo.getUsuario():
                print(" Película: {} ||"
                      " Puntaje Usuarios:  {}".format(titulo.getNombre(),
                                                      usuario.getName()))

    elif(mostrar.upper() == "C"):
        movies = main_de(data_movie, movies, mostrar)
        # Recorre el dataset de movies.
        for titulo in movies:
            # Recorremos los atributos que hay en clase Titulo.
            for critico in titulo.getCritico():
                print(" Película: {} ||"
                      " Puntaje Críticos: {}".format(titulo.getNombre(),
                                                     critico.getName()))

    else:
        print("\n")
        print(" Por favor, ingrese una opción válida")
        listar_pelicula_y(movies, data_movie)

    print("\n")
    print(" I: Ingresar otro filtro")
    print(" M: Volver al menú principal")
    print(" Presione cualquier otra tecla para salir\n")
    opc = str(input(" Ingrese su elección: "))

    if(opc.upper() == "I"):
        listar_pelicula_y(movies, data_movie)

    eleccion(opc)


# Crea la lista con todos los géneros del dataset sin que estos se repitan.
def lista_genero_peliculas(data_genre):

    generos = []

    for index, row in data_genre.iterrows():
        genero = row["genre"].split(",")
        # Bandera que nos servirá para determinar si el
        # género existe o no en el dataset.
        flag = False

        for genre in genero:
            tipo = genre.strip()

            if not generos:
                # Creación del primer género.
                objeto_genero = Generos()
                objeto_genero.setGenero(tipo.capitalize())
                generos.append(tipo.capitalize())

            else:
                for i in generos:
                    # Comparación para ver si existen coincidencias.
                    if(i == genre):
                        # Se rompe el ciclo si el género ya existe.
                        flag = True
                        break

                # Si el género no ha sido ingresado, se ingresa.
                if not flag:
                    objeto_genero = Generos()
                    objeto_genero.setGenero(tipo.capitalize())
                    generos.append(tipo.capitalize())

    # Retorna lista con los géneros
    return generos


# Función que muestra las opciones disponibles en el menú.
def menu():

    # Llamado de funciones para la correcta inicialización del código.
    data = process_file()
    # Se filtra el archivo 'csv'.
    data_movie = data.filter(["title", "director", "genre", "year",
                              "reviews_from_users", "reviews_from_critics"])
    # Obtención de la columna title.
    title_movie = data_movie["title"]
    movies = main_peliculas(title_movie)
    # Creación del subdataset de genre.
    genre_movie = data_movie.filter(["genre"])

    while True:
        borrar()
        print("\t Bienvendios a Movies Encarta ICB\n")
        print(" M: Mostrar Películas y ... ")
        print(" G: Mostrar géneros")
        print(" S: Para Salir\n")
        opcion_movie = str(input(" Ingrese una opción del menú: "))

        # Condicionales del menú.
        if(opcion_movie.upper() == 'M'):
            listar_pelicula_y(movies, data_movie)

        elif(opcion_movie.upper() == "G"):
            lista_generos = lista_genero_peliculas(genre_movie)
            generos = eliminar_repetidos(lista_generos)
            # Validación de la clase con variable.
            genero = Generos()
            # Instancia de la clase.
            genero.setGenero(generos)

            borrar()
            print(" Los géneros de las películas son: \n")
            # Impresión de los datos individualmente.
            for genre in genero.getGenero():
                print("", genre)

            print("\n")
            print(" M: Volver al menú principal")
            print(" Presione cualquier otra tecla para salir\n")
            opc = str(input(" Ingrese su elección: "))
            eleccion(opc)

        elif(opcion_movie.upper() == 'S'):
            print(" Gracias por preferirnos!...")
            borrar()
            exit()

        else:
            print(" Opción inválida....")
            borrar()


if __name__ == '__main__':
    menu()
