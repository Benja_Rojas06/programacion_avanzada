#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from director import Director
from year import Year
from generos import Generos
from usuarios import Usuario
from criticos import Critico


class Titulo:


    # Constructor.
    def __init__(self):
        self._nombre = None
        self._director = []
        self._anio = []
        self._generos = []
        self._usuario = []
        self._critico = []


    # Métodos get y set para cada atributo.
    def getNombre(self):
        return self._nombre


    def setNombre(self, nombre):
        if isinstance(nombre,str):
            self._nombre = nombre


    def getDirector(self):
        return self._director


    def setDirector(self, director):
        if isinstance(director, Director):
            self._director.append(director)


    def getUsuario(self):
        return self._usuario


    def setUsuario(self, opinion):
        if isinstance(opinion, Usuario):
            self._usuario.append(opinion)


    def getGenero(self):
        return self._generos


    def setGenero(self, genero):
        if isinstance(genero, Generos):
            self._generos.append(genero)


    def getCritico(self):
        return self._critico


    def setCritico(self, critico):
        if isinstance(critico, Critico):
            self._critico.append(critico)


    def getAnio(self):
        return self._anio


    def setAnio(self, anio):
        if isinstance(anio, Year):
            self._anio.append(anio)
