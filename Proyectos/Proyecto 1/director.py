#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Director:


    # Constructor.
    def __init__(self, nombre):
        self._nombre = nombre


    # Métodos get.
    def getNombre(self):
        return self._nombre


    # Métodos set.
    def setNombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
