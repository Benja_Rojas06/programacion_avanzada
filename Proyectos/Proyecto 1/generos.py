#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Generos:


    # Constructor.
    def __init__(self):
        self._genero = None


    # Método get.
    def getGenero(self):
        return self._genero


    # Método set.
    def setGenero(self, genero):
        self._genero = genero
