#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Year:


    # Constructor.
    def __init__(self, year):
        self._year = year


    # Método get.
    def getYear(self):
        return self._year


    # Método set.
    def setYear(self, year):
        if isinstance(year, int):
            self._year = year
